from django.http import Http404


def cloud_task_only(handler):
    def check_if_cloud_task(request, *args, **kwargs):
        if request.META.get('HTTP_X_APPENGINE_TASKNAME') is None:
            raise Http404
        if request.META.get('HTTP_X_APPENGINE_QUEUENAME') is None:
            raise Http404

        return handler(request, *args, **kwargs)

    return check_if_cloud_task
