import logging

from apps.taskqueues.tasks_queues import TASK_QUEUES

from google.cloud import tasks_v2beta3

from src import settings


def trigger_backend_task(url, location="us-west2",
                         queue=TASK_QUEUES.DEFAULT, payload=None):
    """

    :type url: str
    :type location: str
    :type queue: str
    :type payload: str
    :return:
    """
    client = tasks_v2beta3.CloudTasksClient()
    project = settings.APPLICATION_ID
    parent = client.queue_path(project, location, queue)

    task = {
        'app_engine_http_request': {  # Specify the type of request.
            'http_method': 'POST',
            'relative_uri': url
        }
    }
    if payload is not None:
        converted_payload = payload.encode()
        task['app_engine_http_request']['body'] = converted_payload

    response = client.create_task(parent, task)

    logging.info('Created task {}'.format(response.name))
    return response
