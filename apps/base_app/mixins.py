from django.db import models
from django.db.models import Max, Min, F
from django.utils.module_loading import import_string
from django.utils.translation import gettext_lazy as _


class OrderingMixin(models.Model):

    order_field_name = None
    order_with_respect_to = None
    order_class_path = None

    class Meta:
        abstract = True

    def _get_class_for_ordering_queryset(self):
        if self.order_class_path:
            return import_string(self.order_class_path)
        return self.__class__

    def _get_order_with_respect_to(self):
        if isinstance(self.order_with_respect_to, str):
            self.order_with_respect_to = (self.order_with_respect_to,)
        if self.order_with_respect_to is None:
            raise AssertionError(
                ('ordered model admin "{0}" has not specified "order_with_respect_to"; note that this '
                 'should go in the model body, and is not to be confused with the Meta property of the same name, '
                 'which is independent Django functionality').format(self))

    def get_ordering_queryset(self, qs=None):
        qs = qs or self._get_class_for_ordering_queryset().objects.all()
        order_with_respect_to = self.order_with_respect_to
        if order_with_respect_to:
            order_values = self._get_order_with_respect_to()
            qs = qs.filter(**dict(order_values))
        return qs

    def to(self, order, qs=None, extra_update=None):
        """
        Move object to a certain position, updating all affected objects to move accordingly up or down.
        """

        if not isinstance(order, int):
            raise TypeError(
                "Order value must be set using an 'int', not using a '{0}'.".format(
                    type(order).__name__))

        if order is None or getattr(self, self.order_field_name) == order:
            # object is already at desired position
            return

        qs = qs or self.get_ordering_queryset()
        if getattr(self, self.order_field_name) > order:
            update_kwargs = {
                self.order_field_name: F(
                    self.order_field_name) + 1}
            if extra_update:
                update_kwargs.update(extra_update)
            qs.filter(**{self.order_field_name + '__lt': getattr(self,
                                                                 self.order_field_name),
                         self.order_field_name + '__gte': order}) .update(**update_kwargs)
        else:
            update_kwargs = {
                self.order_field_name: F(
                    self.order_field_name) - 1}
            if extra_update:
                update_kwargs.update(extra_update)
            qs.filter(**{self.order_field_name + '__gt': getattr(self,
                                                                 self.order_field_name),
                         self.order_field_name + '__lte': order}) .update(**update_kwargs)
        setattr(self, self.order_field_name, order)
        self.save()
