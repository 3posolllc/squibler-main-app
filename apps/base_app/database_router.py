from django.conf import settings


def print_result(func):
    def wrapper(root, *args, **kwargs):
        result = func(root, *args, **kwargs)
        return result
    return wrapper


class DefaultRouter:
    """ Object for switching between databases """

    @classmethod
    def _get_db(cls, app_label):
        try:
            return settings.DATABASE_APPS_MAPPING[app_label]
        except Exception as e:
            return None

    def db_for_read(self, model, **hints):
        """ Get database for read """
        return self._get_db(model._meta.app_label)

    def db_for_write(self, model, **hints):
        """ Get database for read """
        return self._get_db(model._meta.app_label)

    @print_result
    def allow_relation(self, obj1, obj2, **hints):
        """ Determine if relationship is allowed between two objects """
        db1 = self._get_db(obj1._meta.app_label)
        db2 = self._get_db(obj2._meta.app_label)
        return db1 == db2 if db1 and db2 else None

    # @print_result
    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """ Ensure that the app's models get created on the right database """
        if db in settings.DATABASE_APPS_MAPPING.values():
            return settings.DATABASE_APPS_MAPPING[app_label] == db
        elif app_label in settings.DATABASE_APPS_MAPPING:
            return False
        return None
