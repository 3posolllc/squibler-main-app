from os.path import join
from datetime import datetime

from django.conf import settings

ENVIRONMENT_NAME = settings.ENVIRONMENT_NAME


class UploadObject:

    def __init__(self, method_name, field_name, path):
        path += "/%s"

        def upload_to(instance, filename):
            """ Set path to upload images """
            return join(path % getattr(instance, field_name), filename)
        setattr(self, method_name, upload_to)


def create_segment_data(user_uuid,
                        google_analytics_id=None, **kwargs):

    now = datetime.utcnow()
    dateTime = now.strftime("%d, %b %Y %H:%M:%S")

    integrations = {
        'Amplitude': {
            'session_id': int(now.timestamp())
        }
    }
    if google_analytics_id:
        integrations['Google Analytics'] = {
            'clientId': google_analytics_id
        }

    data = {
        'dateTime': dateTime,
        'userId': user_uuid,
        'environment': ENVIRONMENT_NAME,
        **kwargs
    }

    return data, integrations
