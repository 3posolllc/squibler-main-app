class SubSectionOutput:
    def __init__(self, sub_section, created):
        self.sub_section = sub_section
        self.created = created


class NoteOutput:
    def __init__(self, note, created):
        self.note = note
        self.created = created


class BoardOutput:
    def __init__(self, board, created, notes_output):
        self.board = board
        self.created = created
        self.notes_output = notes_output


class SectionOutput:
    def __init__(self, section, created, sub_sections_output, boards_output):
        self.section = section
        self.created = created
        self.sub_sections_output = sub_sections_output
        self.boards_output = boards_output


class ProjectOutput:
    def __init__(self, project, created, sections_output, boards_output):
        self.project = project
        self.created = created
        self.sections_output = sections_output
        self.boards_output = boards_output
