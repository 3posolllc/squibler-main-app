from django.utils.deprecation import MiddlewareMixin


class CookieMiddleware(MiddlewareMixin):

    def process_request(self, request):
        try:
            _ga = request.COOKIES['_ga']
            google_analytics_id = _ga.split('GA1.3.')[1]
        except KeyError:
            google_analytics_id = None

        request.google_analytics_id = google_analytics_id
