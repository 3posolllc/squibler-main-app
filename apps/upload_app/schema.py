from graphene_file_upload.scalars import Upload
import graphene
from apps.jwt_auth.decorators import login_required
from apps.users_app.models import User
from apps.projects_app.models import Project
from apps.boards_app.models import Board
from google.cloud import storage
from django.utils.crypto import get_random_string
from datetime import datetime

storage_client = storage.Client.from_service_account_json(
    'src/keys/sqiblify-08f18b0cbb5a.json')

bucket_name = "squibler_bucket"  # TODO: remove hardcode

apps = {
    'project': Project,
    'board': Board,
}


def generate_file_name():
    return "%s_%s.png" % (get_random_string(), str(datetime.utcnow()))


class Upload(graphene.Mutation):
    class Arguments:
        file = Upload(required=True)
        target = graphene.String(required=True)
        uuid = graphene.String(required=False)

    ok = graphene.Boolean()
    error = graphene.String()
    url = graphene.String()

    @staticmethod
    @login_required
    def mutate(_, info, file, target, uuid):
        try:
            user = info.context.user

            print(file)
            print(type(file))
            print(dir(file))

            if target == 'user':
                result_object = user
            else:
                result_object = apps.get(target).objects.get(
                    user_id=user.id, id=uuid)

            bucket = storage_client.get_bucket(bucket_name)
            blob = bucket.blob(generate_file_name())
            blob.upload_from_string(file)
            blob.make_public()

            if result_object.have_image():
                bucket = storage_client.get_bucket(bucket_name)
                blob = bucket.blob(result_object.get_image_name())
                blob.delete()

            result_object.set_image(blob.public_url, blob.name)
            result_object.save()

            return Upload(ok=True, error=None, url=blob.public_url)

        except Exception as e:
            return Upload(ok=False, error=str(e))


class ProjectMutations(graphene.ObjectType):
    upload = UploadMutation.Field()
