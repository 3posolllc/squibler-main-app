from django.http import JsonResponse
from apps.jwt_auth.decorators import login_required, login_required_simple
from django.core.files.storage import default_storage
from django.utils.crypto import get_random_string
from datetime import datetime


@login_required_simple
def upload(request):
    if request.method == 'POST':
        file_source = request.FILES.get('file')
        file_name = "%s_%s.%s" % (get_random_string(
            6), datetime.utcnow().timestamp(), file_source.name.split('.')[-1])
        file = default_storage.open(
            '/photos/users/%s/%s' %
            (request.user.id, file_name), 'w')
        file.write(file_source.read())
        file.close()
        file.blob.make_public()
        return JsonResponse({'link': file.blob.public_url}, status=200)
    else:
        return JsonResponse(status=405)
