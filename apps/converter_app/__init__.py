import os
from django.utils.text import slugify
from django.conf import settings


def convert(name, html_data, file_format):
    name = slugify(name)
    file_input_path = os.path.join(
        settings.PATH_TO_SAVED_FILES, name + '.html')
    file_output_path = os.path.join(
        settings.PATH_TO_SAVED_FILES,
        name + '.' + file_format)
    f = open(file_input_path, 'w+')
    f.write(html_data)
    f.close()
    os.system('ebook-convert %s %s' % (file_input_path, file_output_path))
    return file_output_path
