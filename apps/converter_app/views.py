from io import FileIO, BytesIO

import requests
from django.shortcuts import render
from django.conf import settings
from django.http import JsonResponse, HttpResponse
from apps.jwt_auth.decorators import login_required_simple
from apps.projects_app.models import Project
from . import convert
from wsgiref.util import FileWrapper


CONVERTER_URL = settings.CONVERTER_URL


@login_required_simple
def export_file(request, uuid, file_format):
    if request.method == 'GET':
        user = request.user
        project = Project.objects.get(uuid=uuid, user_id=user.id)
        name = "".join(project.title.lower().split(" "))

        response = requests.post(
            CONVERTER_URL,
            {
                "html_content": project.get_pagination_html(),
                "format_type": file_format,
                "user_id": user.id,
            },
            headers={
                "Authorization": "JWT %s" % user._token
            }
        )

        if response.status_code == 200:
            output = HttpResponse(
                BytesIO(response.content),
                content_type='application/%s' %
                file_format)
            output['Content-Disposition'] = 'attachment; filename="%s.%s"' % (
                name, file_format)
            return output
        else:
            return JsonResponse({
                'status': 'error',
                'message': response.reason
            }, status=response.status_code)
    else:
        return JsonResponse({}, status=405)
