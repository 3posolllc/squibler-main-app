from .models import Board, Note
import graphene
from apps.jwt_auth.decorators import login_required
from graphene_django.types import DjangoObjectType
from apps.jwt_auth.decorators import login_required, payment_required
from apps.base_app.types import NoteOutput, BoardOutput
from .tasks import board_deleted


class NoteType(DjangoObjectType):
    uuid = graphene.String()
    created = graphene.Boolean()
    created_at = graphene.String()
    updated_at = graphene.String()

    class Meta:
        model = Note
        interfaces = (graphene.relay.Node, )
        only_fields = ('uuid', 'title', 'text')


class NoteOutputType(graphene.ObjectType):
    note = graphene.Field(NoteType)
    created = graphene.Boolean()


class BoardType(DjangoObjectType):
    notes = graphene.List(NoteType)
    uuid = graphene.String()
    created_at = graphene.String()
    updated_at = graphene.String()

    class Meta:
        model = Board
        interfaces = (graphene.relay.Node, )
        only_fields = ('name', 'summary', 'notes',
                       'created_at', 'updated_at', 'color', 'icon_name')


class BoardOutputType(graphene.ObjectType):
    board = graphene.Field(BoardType)
    created = graphene.Boolean()
    notes_output = graphene.List(NoteOutputType)


class NoteInput(graphene.InputObjectType):
    uuid = graphene.String(required=False)
    title = graphene.String(required=False)
    text = graphene.String(required=False)
    board_id = graphene.String(required=False)


class BoardInput(graphene.InputObjectType):
    uuid = graphene.String(required=False)
    name = graphene.String(required=False)
    summary = graphene.String(required=False)
    notes = graphene.List(NoteInput, required=False)
    icon_name = graphene.String(required=False)
    icon_url = graphene.String(required=False)
    color = graphene.String(required=False)


class DeleteBoard(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=True)

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    @login_required
    @payment_required
    def mutate(_, info, uuid):
        try:
            user = info.context.user

            board = Board.objects.get(id=uuid, user_id=user.id)
            board.delete()
            return DeleteBoard(ok=True, error=None)
        except Exception as e:
            return DeleteBoard(ok=False, error=str(e))


class DeleteNote(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=True)

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    @login_required
    @payment_required
    def mutate(_, info, uuid):
        try:
            user = info.context.user

            Note.objects.get(
                board__in=Board.objects.filter(
                    user_id=user.id),
                id=uuid).delete()
            return DeleteNote(ok=True, error=None)
        except Exception as e:
            return DeleteNote(ok=False, error=str(e))


class CreateOrUpdateBoards(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=False)
        name = graphene.String(required=False)
        summary = graphene.String(required=False)
        notes = graphene.List(NoteInput, required=False)
        icon_name = graphene.String(required=False)
        icon_url = graphene.String(required=False)
        color = graphene.String(required=False)

    ok = graphene.Boolean()
    error = graphene.String()
    board_output = graphene.Field(BoardOutputType)

    @staticmethod
    @login_required
    @payment_required
    def mutate(_, info, **board_input):
        try:
            user = info.context.user

            board_output = BoardOutput(
                *
                Board.update_or_create(
                    **board_input,
                    user_id=user.id))

            return CreateOrUpdateBoards(
                board_output=board_output,
                ok=True,
                error=None,
            )
        except Exception as e:
            return CreateOrUpdateBoards(
                ok=False,
                error=str(e),
            )


class BoardMutations(graphene.ObjectType):
    create_or_update_boards = CreateOrUpdateBoards.Field()
    delete_board = DeleteBoard.Field()
    delete_note = DeleteNote.Field()


class BoardQueries(graphene.ObjectType):
    boards = graphene.List(BoardType)
    board = graphene.Field(BoardType, uuid=graphene.String(required=True))

    @login_required
    @payment_required
    def resolve_boards(self, info, **kwargs):
        user = info.context.user
        return Board.objects.filter(
            user_id=user.id,
            section_id=None,
            project_id=None).order_by('_created_at')

    @login_required
    @payment_required
    def resolve_board(self, info, *args, **kwargs):
        user = info.context.user

        return Board.objects.filter(id=kwargs['uuid'], user_id=user.id).first()
