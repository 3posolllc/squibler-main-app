from datetime import datetime

import logging
from django.conf import settings
import analytics

from apps.base_app.helpers import create_segment_data

analytics.write_key = settings.SEGMENT_API_KEY
ENVIRONMENT_NAME = settings.ENVIRONMENT_NAME

logger = logging.getLogger(__name__)


def board_created(user_uuid, board_id, google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            boardId=board_id,
        )

        analytics.track(user_uuid, 'Board created', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Board created error")


def board_updated(user_uuid, board_id, google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            boardId=board_id,
        )
        analytics.track(user_uuid, 'Board updated', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Board updated exception")


def board_deleted(user_uuid, board_id, google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            boardId=board_id,
        )

        analytics.track(user_uuid, 'Board deleted', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Board deleted error")


def note_created(user_uuid, board_id, note_id,
                 google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            boardId=board_id,
            noteId=note_id,
        )
        analytics.track(user_uuid, 'Note created', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.error("Note created error")


def note_updated(user_uuid, board_id, note_id,
                 google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            boardId=board_id,
            noteId=note_id,
        )
        analytics.track(user_uuid, 'Note updated', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Note updated")


def note_moved(user_uuid, board_id, note_id, new_board_id,
               google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            boardId=board_id,
            noteId=note_id,
            newBoardId=new_board_id,
        )
        analytics.track(user_uuid, 'Note moved', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Note moved error")
