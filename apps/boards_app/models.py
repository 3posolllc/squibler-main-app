from django.db import models
from django.utils.translation import gettext as _
import uuid
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from apps.base_app.types import NoteOutput
from .tasks import board_created, board_updated, note_created, \
    note_updated, note_moved


class Note(models.Model):
    """ Note model """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(_("Title"), max_length=256, blank=True)
    text = models.TextField(_("Text"))

    board = models.ForeignKey("boards_app.Board", on_delete=models.CASCADE)

    _created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    _updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _('note')
        verbose_name_plural = _('notes')
        ordering = ('_created_at',)
        app_label = 'boards_app'

    def __str__(self):
        return self.title

    @property
    def uuid(self):
        return str(self.id)

    @property
    def timestamp(self):
        return self.created_at.timestamp()

    @property
    def created_at(self):
        return str(self._created_at)

    @property
    def updated_at(self):
        return str(self._updated_at)

    @classmethod
    def update_or_create(cls, **kwargs):
        current_board_id = kwargs.pop('current_board_id')
        uuid = kwargs.pop('uuid', None)
        user_id = kwargs.pop('user_id')
        if uuid:
            notes = cls.objects.filter(board_id=current_board_id, id=uuid)
            new_board_id = kwargs.pop('board_id', None)
            if kwargs:
                notes.update(**kwargs)
            note = notes.first()

            if new_board_id:
                note.board = Board.objects.get(id=new_board_id)

            note.save()
            created = False
        else:
            note = cls.objects.create(**kwargs, board_id=current_board_id)
            note.save()
            created = True
        return note, created


class Board(models.Model):
    """ Board model """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(_("Name"), max_length=256, blank=True)
    summary = models.TextField(_("Summary"), blank=True)
    icon_name = models.CharField(
        _("Icon name"),
        max_length=256,
        blank=True,
        null=True)
    icon_url = models.CharField(
        _("Icon url"),
        max_length=2084,
        blank=True,
        null=True)
    color = models.CharField(_("Color"), max_length=9, null=True, blank=True)

    section_id = models.UUIDField(blank=True, editable=True, null=True)
    project_id = models.CharField(
        blank=True,
        editable=True,
        null=True,
        max_length=256)
    user_id = models.UUIDField(
        blank=False,
        editable=False,
        null=False,
        default='00000000-0000-0000-0000-000000000000')

    _created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    _updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _('board')
        verbose_name_plural = _('boards')
        app_label = 'boards_app'
        ordering = ('_created_at',)

    @property
    def notes(self):
        return Note.objects.filter(board=self)

    @property
    def uuid(self):
        return str(self.id)

    @property
    def icon(self):
        return self.icon_url

    @property
    def timestamp(self):
        return self.created_at.timestamp()

    def __str__(self):
        return self.name

    @property
    def created_at(self):
        return str(self._created_at)

    @property
    def updated_at(self):
        return str(self._updated_at)

    def have_image(self):
        return True if self.icon_name else False

    def get_image_name(self):
        return self.icon_name

    def set_image(self, url, name):
        self.icon_url = url
        self.icon_name = name
        self.save()

    @classmethod
    def update_or_create(cls, **kwargs):
        notes_json = kwargs.pop('notes', [])

        if kwargs.get('uuid'):
            board = cls.objects.filter(
                id=kwargs.pop('uuid'),
                user_id=kwargs.pop('user_id'),
            )
            board.update(**kwargs)
            board = board.first()

            if kwargs.get('color', None):
                board.color = kwargs.get('color')
                board.icon_name = None
            if kwargs.get('icon_name', None):
                board.icon_name = kwargs.get('icon_name')
                board.color = None
            board.save()
            created = False

        else:
            board = cls.objects.create(**kwargs)
            board.save()
            created = True

        notes_out = [
            NoteOutput(
                *
                Note.update_or_create(
                    **nj,
                    current_board_id=board.id,
                    user_id=board.user_id)) for nj in notes_json]

        return board, created, notes_out

    @classmethod
    def copy_many(
        cls,
        old_project_id=None,
        new_project_id=None,
        old_section_id=None,
        new_section_id=None
    ):

        for board in cls.objects.filter(
                project_id=old_project_id, section_id=old_section_id):
            old_board_id = board.pk
            old_notes = board.notes
            board.pk = None
            board.project_id = new_project_id
            board.section_id = new_section_id
            board.save()

            for old_note in old_notes:
                old_note.board = board
                old_note.pk = None
                old_note.save()
