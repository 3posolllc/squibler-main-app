from rest_framework import serializers
from .models import Board, Note


class NoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Note
        fields = ('uuid', 'title', 'text', 'created_at', 'updated_at')


class BoardSerializer(serializers.ModelSerializer):

    @property
    def notes(self):
        return NoteSerializer(
            Note.objects.filter(board=self.instance),
            many=True
        )

    class Meta:
        model = Board
        fields = ('uuid', 'name', 'summary', 'notes',
                  'created_at', 'updated_at', 'icon', 'color')
