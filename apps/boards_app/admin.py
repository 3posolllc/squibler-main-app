from django.contrib import admin
from .models import Board, Note


class BoardAdmin(admin.ModelAdmin):
    class Meta:
        model = Board


admin.site.register(Board, BoardAdmin)


class NoteAdmin(admin.ModelAdmin):
    class Meta:
        model = Note


admin.site.register(Note, NoteAdmin)
