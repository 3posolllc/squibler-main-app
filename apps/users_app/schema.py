import graphene
import stripe
from django.db.utils import IntegrityError
from django.conf import settings
from django.utils.crypto import get_random_string
from graphene_file_upload.scalars import Upload
from graphene_django.types import DjangoObjectType
from apps.jwt_auth.decorators import login_required, payment_check
from .models import User, StripeCustomer, UserProfile, ConfirmCode
from .tasks import billing_error, user_login, user_logout, trial_expired
from .social_auth import (
    GoogleAuthService,
    FacebookAuthService,
    TokenValidationError
)


class UserCreateInput(graphene.InputObjectType):
    email = graphene.String(required=True)
    password = graphene.String(required=True)
    time_spend = graphene.String(required=True)
    book_type = graphene.String(required=True)
    name = graphene.String(required=False)


class UserUpdateInput(graphene.InputObjectType):
    email = graphene.String(required=False)
    password = graphene.String(required=False)
    old_password = graphene.String(required=False)
    name = graphene.String(required=False)
    file = Upload(required=False)


class Payment(graphene.ObjectType):
    amount = graphene.Int()
    timestamp = graphene.Int()
    paid = graphene.Boolean()
    status = graphene.String()


class PaymentInfoType(graphene.ObjectType):
    card_number = graphene.String()
    user_email = graphene.String()
    is_active = graphene.Boolean()
    status = graphene.String()
    exp_month = graphene.String()
    exp_year = graphene.String()
    current_period_end = graphene.Int()
    payment_history = graphene.List(Payment)


class UserType(DjangoObjectType):
    uuid = graphene.String()
    payment_info = graphene.Field(PaymentInfoType)
    token = graphene.String()

    class Meta:
        model = User
        interfaces = (graphene.relay.Node, )
        only_fields = (
            'uuid',
            'email',
            'updated_at',
            'created_at',
            'is_active',
            'deleted_date',
            'profile',
            'created_by_google',
            'created_by_facebook',
        )


class UserProfileType(DjangoObjectType):
    uuid = graphene.String()
    photo_url = graphene.String()

    class Meta:
        model = UserProfile
        interfaces = (graphene.relay.Node, )
        only_fields = (
            'uuid', 'name', 'photo_url',
            'photo', 'created_at', 'updated_at',
            'book_type', 'time_spend'
        )


class AddPayment(graphene.Mutation):
    class Arguments:
        card_number = graphene.String()
        card_exp_month = graphene.Int()
        card_exp_year = graphene.Int()
        card_cvv = graphene.Int()

    ok = graphene.Boolean()
    created = graphene.Boolean()
    error = graphene.String()
    message = graphene.String()

    @staticmethod
    @login_required
    def mutate(_, info, **card_input):
        try:
            user = info.context.user
            _, created, message = StripeCustomer.update_or_create(
                **card_input, user=user)
            return AddPayment(
                ok=True,
                error=None,
                created=created,
                message=message
            )
        except stripe.error.CardError as e:
            billing_error(user.email, user.uuid)
            body = e.json_body
            err = body.get('error', {})
            return AddPayment(
                ok=False,
                error=err.get('type'),
                message=err.get('message'),
            )
        except Exception as e:
            return AddPayment(
                ok=False,
                error="Server Error",
                message=str(e),
            )


class CancelPayment(graphene.Mutation):
    class Arguments:
        pass

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    @login_required
    def mutate(_, info):
        user = info.context.user
        stripe_customer = StripeCustomer.find_by_user(user)

        if stripe_customer:
            try:
                stripe_customer.cancel_subscription()
                return CancelPayment(
                    ok=True,
                    error=None
                )
            except Exception as err:
                return CancelPayment(
                    ok=False,
                    error=str(err),
                )
        else:
            return CancelPayment(
                ok=False,
                error="Customer does not exist"
            )


class TrialExpired(graphene.Mutation):
    class Arguments:
        stripe_user_id = graphene.String()

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    def mutate(_, info, stripe_user_id):
        user = info.context.user
        stripe_customer = StripeCustomer.find_by_external_id(stripe_user_id)
        if stripe_customer:
            try:
                user = stripe_customer.user
                trial_expired(user.email, user.uuid)
                return TrialExpired(
                    ok=True,
                    error=None
                )
            except Exception as err:
                return TrialExpired(
                    ok=False,
                    error=str(err),
                )
        else:
            return TrialExpired(
                ok=False,
                error="Customer does not exist"
            )


class GoogleAuth(graphene.Mutation):
    class Arguments:
        google_token = graphene.String(required=True)
        time_spend = graphene.String(required=True)
        book_type = graphene.String(required=True)

    ok = graphene.Boolean()
    token = graphene.String()
    user = graphene.Field(UserType)
    error = graphene.String()
    created = graphene.Boolean()

    @staticmethod
    def mutate(_, info, google_token, time_spend, book_type):
        try:
            payloads = GoogleAuthService.validate_token(google_token)
            email = payloads['email']
            user = User.objects.filter(email=email).first()
            if user:
                return GoogleAuth(
                    ok=True,
                    error=False,
                    token=user._encode_token(),
                    user=user,
                    created=False
                )
            else:
                name = payloads['name']
                photo_url = payloads['picture']

                profile = UserProfile.objects.create(
                    name=name,
                    _photo_url=photo_url,
                    time_spend=time_spend,
                    book_type=book_type,
                )
                profile.save()
                user = User.objects.create(
                    email=email,
                    password=get_random_string(),
                    profile=profile,
                    created_by_google=True
                )
                user.save()
                return GoogleAuth(
                    ok=True,
                    error=False,
                    token=user._encode_token(),
                    user=user,
                    created=True
                )

        except TokenValidationError:
            return FacebookAuth(
                ok=False,
                error="Invalid token"
            )
        except Exception as e:
            return GoogleAuth(
                ok=False,
                error=str(e)
            )


class FacebookAuth(graphene.Mutation):
    class Arguments:
        facebook_token = graphene.String(required=True)
        time_spend = graphene.String(required=True)
        book_type = graphene.String(required=True)

    ok = graphene.Boolean()
    token = graphene.String()
    error = graphene.String()
    user = graphene.Field(UserType)
    created = graphene.Boolean()

    @staticmethod
    def mutate(_, info, facebook_token, time_spend, book_type):
        try:
            payloads = FacebookAuthService.validate_token(facebook_token)
            email = payloads['email']
            user = User.objects.filter(email=email).first()
            if user:
                return FacebookAuth(
                    ok=True,
                    error=False,
                    token=user._encode_token(),
                    user=user,
                    created=False
                )
            else:
                name = payloads['name']
                photo_url = payloads['picture']['data']['url']

                profile = UserProfile.objects.create(
                    name=name,
                    _photo_url=photo_url,
                    time_spend=time_spend,
                    book_type=book_type,
                )
                profile.save()

                user = User.objects.create(
                    email=email,
                    password=get_random_string(),
                    profile=profile,
                    created_by_facebook=True,
                )
                return FacebookAuth(
                    ok=True,
                    error=False,
                    token=user._encode_token(),
                    user=user,
                    created=True
                )

        except TokenValidationError:
            return FacebookAuth(
                ok=False,
                error="Invalid token"
            )
        except Exception as e:
            return FacebookAuth(
                ok=False,
                error=str(e)
            )


class Login(graphene.Mutation):
    class Arguments:
        email = graphene.String(required=True)
        password = graphene.String(required=True)

    ok = graphene.Boolean()
    token = graphene.String()
    error = graphene.String()
    user = graphene.Field(UserType)

    @staticmethod
    def mutate(_, info, email, password):
        try:
            user, token = User.login(email, password)
            user.payment_info = StripeCustomer.get_pinfo_by_user(user)
            return Login(
                token=token,
                ok=True,
                error=None,
                user=user,
            )
        except Exception as e:
            return Login(
                ok=False,
                error=str(e),
            )


class Logout(graphene.Mutation):
    class Arguments:
        pass

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    @login_required
    def mutate(_, info):
        try:
            user = info.context.user
            return Login(
                ok=True,
                error=None,
            )
        except Exception as e:
            return Login(
                ok=False,
                error=str(e),
            )


class ForgotPassword(graphene.Mutation):
    class Arguments:
        email = graphene.String(required=True)

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    def mutate(_, info, email):
        try:
            user = User.objects.filter(email=email).first()
            if not user:
                ok = False
                error = "User not found"
            else:
                ConfirmCode.create(user=user)
                ok = True
                error = None
            return ForgotPassword(
                ok=ok,
                error=error
            )
        except Exception as e:
            return ForgotPassword(
                ok=False,
                error=str(e)
            )


class RestorePassword(graphene.Mutation):
    class Arguments:
        code = graphene.String(required=True)
        new_password = graphene.String(required=True)

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    def mutate(_, info, code, new_password):
        try:
            confirm_code = ConfirmCode.objects.filter(
                body=code, activated=False).first()
            if not confirm_code:
                return RestorePassword(
                    ok=False,
                    error="Invalid code"
                )

            confirm_code.user.hash_password(new_password)
            confirm_code.user.save()

            confirm_code.activated = True
            confirm_code.save()
            return RestorePassword(
                ok=True,
                error=None
            )
        except Exception as e:
            return ForgotPassword(
                ok=False,
                error=str(e)
            )


class CreateUser(graphene.Mutation):
    class Arguments:
        user_create_input = UserCreateInput(required=True)

    ok = graphene.Boolean()
    user = graphene.Field(UserType)
    token = graphene.String()
    error = graphene.String()

    @staticmethod
    def mutate(_, info, user_create_input):

        try:
            profile = UserProfile.objects.create(
                book_type=user_create_input.pop('book_type'),
                time_spend=user_create_input.pop('time_spend'),
                name=user_create_input.pop('name', ""),
            )
            profile.save()
            user = User.objects.create(**user_create_input, profile=profile)
            user.save()
            return CreateUser(
                user=user,
                ok=True,
                token=user._encode_token(),
                error=None
            )
        except IntegrityError:
            return CreateUser(
                ok=False,
                error="User already exist",
            )


class UpdateUser(graphene.Mutation):
    class Arguments:
        user_update_input = UserUpdateInput(required=True)

    ok = graphene.Boolean()
    user = graphene.Field(UserType)
    token = graphene.String()
    error = graphene.String()

    @staticmethod
    @login_required
    def mutate(_, info, user_update_input):
        try:
            user = info.context.user
            user.update(**user_update_input)
            user.save()
            user.payment_info = StripeCustomer.get_pinfo_by_user(user)

            return UpdateUser(
                user=user,
                ok=True,
                token=user._encode_token(),
                error=None
            )
        except Exception as e:
            return UpdateUser(
                ok=False,
                error=str(e),
            )


class UserMutations(graphene.ObjectType):
    create_user = CreateUser.Field()
    update_user = UpdateUser.Field()
    login = Login.Field()
    logout = Logout.Field()
    forgot_password = ForgotPassword.Field()
    restore_password = RestorePassword.Field()
    google_auth = GoogleAuth.Field()
    facebook_auth = FacebookAuth.Field()
    add_payment = AddPayment.Field()
    cancel_payment = CancelPayment.Field()


class UserQueries(graphene.ObjectType):
    user = graphene.Field(UserType)

    @login_required
    @payment_check
    def resolve_user(self, info, **kwargs):
        user = info.context.user
        user.payment_info = kwargs.get('payment_info')
        return user
