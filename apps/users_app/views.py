from django.shortcuts import render
from django.http import JsonResponse
from .tasks import test_event


def test_google_event(request):
    if request.method == 'POST':
        data = {key: value for key, value in request.POST.items()}

        test_event(**data)
        return JsonResponse(data, status=200)
    else:
        return JsonResponse(status=405)
