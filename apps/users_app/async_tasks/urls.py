from django.urls import path

from . import async_views

urlpatterns = [
    path('register_user_async', async_views.register_user_async),
]