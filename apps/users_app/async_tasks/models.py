import json


class RegisterUserAsyncDTO(object):

    def __init__(self, uuid=None, email=None, first_name=None, last_name=None):
        """

        :type uuid: str
        :type email: str
        :type first_name: str
        :type last_name: str
        """
        self.uuid = uuid
        self.email = email
        self.first_name = first_name
        self.last_name = last_name

    def to_json_string(self):
        """
        Returns json string representation of dto class
        :rtype: str
        """
        return json.dumps(
            dict(
                uuid=self.uuid,
                email=self.email,
                first_name=self.first_name,
                last_name=self.last_name,
            )
        )

    @classmethod
    def from_json_string(cls, data):
        """

        :rtype: RegisterUserAsyncDTO
        """
        parsed_data = json.loads(data)
        return cls(
            uuid=parsed_data["uuid"],
            email=parsed_data["email"],
            first_name=parsed_data["first_name"],
            last_name=parsed_data["last_name"]
        )
