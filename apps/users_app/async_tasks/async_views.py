from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
import analytics

from django.http import HttpResponse

from apps.taskqueues.decorators import cloud_task_only
from apps.users_app.async_tasks.models import RegisterUserAsyncDTO
from apps.users_app.tasks import register_user

analytics.write_key = settings.SEGMENT_API_KEY
ENVIRONMENT_NAME = settings.ENVIRONMENT_NAME


@csrf_exempt
@cloud_task_only
def register_user_async(request):
    rua_dto = RegisterUserAsyncDTO.from_json_string(
        request.body.decode("utf-8"))

    register_user(rua_dto)

    return HttpResponse("ok")
