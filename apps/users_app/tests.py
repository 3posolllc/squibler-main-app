from django.test import TestCase
from .models import User, UserProfile, ConfirmCode, StripeCustomer


class UserTest(TestCase):

    def test_create_valid_user(self):
        try:
            user = User.objects.create(
                email=self.valid_email,
                password=self.valid_password
            )
            user.save()
            ok = True
        except Excption as e:
            ok = False
        self.assertTrue(ok)

    def test_create_invalid_user_email(self):
        try:
            user = User.objects.create(
                email=self.invalid_email,
                password=self.valid_password
            )
            user.save()
            ok = True
        except Excption as e:
            ok = False
        self.assertFalse(ok)

    def test_get_user(self):
        user = User.object.get(email=self.valid_email)

        self.assertEqual(user.email, self.valid_email)
        self.assertTrue(user._check_hash(self.valid_password))

    def test_login(self):
        User.login(self.valid_email, self.valid_password)
