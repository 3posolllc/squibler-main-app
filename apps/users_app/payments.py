from enum import Enum


class PaymentStatus(Enum):
    ACTIVE = 'active'
    INACTIVE = 'inactive'
    TRIAL = 'trial'


class Payment:
    def __init__(self, amount, timestamp, paid, status):
        self.amount = amount
        self.timestamp = timestamp
        self.paid = paid
        self.status = status


class PaymentInfo:

    def __init__(self):
        self.card_number = None
        self.user_email = None
        self.is_active = False
        self.status = PaymentStatus.TRIAL.value
        self.exp_month = None
        self.exp_year = None
        self.current_period_end = 0
        self.payment_history = []

    def extract_charges(self, charges):
        for charge in charges:
            payment = Payment(
                charge.amount,
                charge.created,
                charge.paid,
                charge.status
            )
            self.payment_history.append(payment)
        return True

    def __str__(self):
        return """
            card_number: {0},
            user_email: {1},
            is_active: {2},
            status: {3},
            exp_month: {4},
            exp_year: {5},
            current_period_end: {6}
        """.format(
            self.card_number,
            self.user_email,
            self.is_active,
            self.status,
            self.exp_month,
            self.exp_year,
            self.current_period_end
        )

    @staticmethod
    def get_statuses():
        return PaymentStatus

    def add_card(self, card_number, user_email,
                 exp_month, exp_year, current_period_end):
        self.card_number = card_number
        self.user_email = user_email
        self.is_active = True
        self.status = self.get_statuses().ACTIVE.value
        self.exp_month = exp_month
        self.exp_year = exp_year
        self.current_period_end = current_period_end
        return self

    def deactivate(self):
        self.is_active = False
        self.status = self.get_statuses().INACTIVE.value
        self.exp_month = None
        self.exp_year = None
        return self

    def set_admin(self):
        self.is_active = True
        self.status = self.get_statuses().ACTIVE.value
