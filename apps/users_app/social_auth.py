import requests
from django.conf import settings

FACEBOOK_API_URL = settings.FACEBOOK_API_URL
GOOGLE_API_URL = settings.GOOGLE_API_URL


class TokenValidationError(Exception):
    pass


class AuthService:

    @classmethod
    def validate_token(cls, token):
        url = cls.AUTH_BACKEND_URL.format(token)
        response = requests.get(url)
        if response.status_code == 200:
            return response.json()
        else:
            raise TokenValidationError


class FacebookAuthService(AuthService):

    AUTH_BACKEND_URL = FACEBOOK_API_URL


class GoogleAuthService(AuthService):

    AUTH_BACKEND_URL = GOOGLE_API_URL
