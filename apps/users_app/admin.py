from django.contrib import admin
from .models import User, UserProfile, StripeCustomer, ConfirmCode


class UserAdmin(admin.ModelAdmin):
    class Meta:
        model = User


admin.site.register(User, UserAdmin)


class UserProfileAdmin(admin.ModelAdmin):
    class Meta:
        model = UserProfile


admin.site.register(UserProfile, UserProfileAdmin)


class StripeCustomerAdmin(admin.ModelAdmin):
    class Meta:
        model = StripeCustomer


admin.site.register(StripeCustomer, StripeCustomerAdmin)


class ConfirmCodeAdmin(admin.ModelAdmin):
    class Meta:
        model = ConfirmCode


admin.site.register(ConfirmCode, ConfirmCodeAdmin)
