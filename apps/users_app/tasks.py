from datetime import datetime

import logging
from django.conf import settings
from django.urls import reverse
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
import analytics

from apps.base_app.helpers import create_segment_data
from .auto_pilot import AutopilotAPI

analytics.write_key = settings.SEGMENT_API_KEY
ENVIRONMENT_NAME = settings.ENVIRONMENT_NAME

logger = logging.getLogger(__name__)


def register_user(register_user_async_dto, google_analytics_id=None):
    """

    :type register_user_async_dto: apps.users_app.async_tasks.models.RegisterUserAsyncDTO
    """
    try:
        AutopilotAPI.register_user(
            register_user_async_dto.first_name,
            register_user_async_dto.last_name,
            register_user_async_dto.email
        )

    except BaseException:
        import logging
        logging.exception("Register user error")
    print("Registered user async task done")


def forgot_password(code, user_email):
    AutopilotAPI.forgot_password(code, user_email)


def activate_subscription(date, price, user_email,
                          user_uuid, google_analytics_id=None):
    try:
        AutopilotAPI.activate_subscription(date, price, user_email)
        # data, integrations = create_segment_data(
        #     user_uuid,
        #     google_analytics_id=google_analytics_id,
        #     userEmail=user_email,
        #     price=price,
        # )

        # analytics.track(user_uuid, 'Paid account created', data,
        #     integrations=integrations
        # )
    except BaseException:
        import logging
        logging.exception("Activate subscription error")


def cancel_subscription(user_email, user_uuid,
                        google_analytics_id=None):
    try:
        AutopilotAPI.cancel_subscription(user_email)
        # data, integrations = create_segment_data(
        #     user_uuid,
        #     google_analytics_id=google_analytics_id,
        #     userEmail=user_email,
        # )

        # analytics.track(user_uuid, 'Subscription canceled', data,
        #     integrations=integrations
        # )
    except BaseException:
        import logging
        logging.exception("Cancel Subscription error")


def billing_error(user_email, user_uuid, google_analytics_id=None):
    try:
        AutopilotAPI.billing_error(user_email)
        # data, integrations = create_segment_data(
        #     user_uuid,
        #     google_analytics_id=google_analytics_id,
        #     userEmail=user_email,
        # )

        # analytics.track('Billing error', data,
        #     integrations= integrations
        # )
    except BaseException:
        import logging
        logging.exception("Billing error error")


def trial_expired(user_email, user_uuid, google_analytics_id=None):
    try:
        AutopilotAPI.trial_expired(user_email)
        # data, integrations = create_segment_data(
        #     user_uuid,
        #     google_analytics_id=google_analytics_id,
        #     userEmail=user_email,
        # )

        # analytics.track(user_uuid, 'Trial ended', data,
        #     integrations=integrations
        # )
    except BaseException:
        import logging
        logging.exception("Trial expired error")


def user_login(user_email, user_uuid, google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            userEmail=user_email,
        )

        analytics.track(user_uuid, 'User signed in', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("User login")


def user_logout(user_email, user_uuid, google_analytics_id=None):
    # segment: signed out
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            userEmail=user_email,
        )

        analytics.track(user_uuid, 'User signed out', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("User logout")


def user_updated(user_email, user_uuid, first_name,
                 last_name, google_analytics_id=None):
    try:
        AutopilotAPI.update_user(first_name, last_name, user_email)
        # data, integrations = create_segment_data(
        #     user_uuid,
        #     google_analytics_id=google_analytics_id,
        #     userEmail=user_email,
        # )

        # analytics.track(user_uuid, 'User updated', data,
        #     integrations=integrations
        # )
    except BaseException:
        import logging
        logging.exception("User updated")


def test_event(
        user_email,
        first_name,
        last_name,
        user_uuid,
        user_created_by_google,
        event_name,
        google_analytics_id=None
):
    try:
        now = datetime.utcnow()
        dateTime = now.strftime("%d, %b %Y %H:%M:%S")

        integrations = {
            'Amplitude': {
                'session_id': int(now.timestamp())
            }
        }
        if google_analytics_id:
            integrations['Google Analytics'] = {
                'clientId': google_analytics_id
            }

        analytics.track(user_uuid, event_name, {
            'dateTime': dateTime,
            'userId': user_uuid,
            'userEmail': user_email,
            'environment': ENVIRONMENT_NAME,
        },
            integrations=integrations)
    except BaseException:
        import logging
        logging.exception("Test Event Error")
