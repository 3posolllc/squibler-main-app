import requests
import json
from enum import Enum

from django.conf import settings

_AUTOPILOT_API_KEY = settings.AUTOPILOT_API_KEY
_AUTOPILOT_API_UPDATE = settings.AUTOPILOT_API_UPDATE
# % (trigger_id, user_email)
_AUTOPILOT_API_TRIGGER = settings.AUTOPILOT_API_TRIGGER
_AUTOPILOT_BACK_URL = settings.AUTOPILOT_BACK_URL

_DEFAULT_DATA = {
    "contact": {
        "Email": "mail@mail.com",
        "FirstName": "anon",
        "LastName": "",
        "custom": {
                    "string--PlanType": "Annual",
                    "float--Price": 10,
                    "string--PaymentDate": "",  # 2018-11-14T15:20:30.45+01:00
            "string--DomainName": _AUTOPILOT_BACK_URL,
            "string--ForgotPasswordCode": ""
        }
    }
}


class _TriggerKeys(Enum):
    REGISTER = '0004'
    FORGOT_PASSWORD = '0006'
    ACTIVATION = '0005'
    CANCELED = '0003'
    BILLING_ERROR = '0001'
    TRIAL_EXPIRED = '0002'


_headers = {
    "autopilotapikey": _AUTOPILOT_API_KEY,
    'content-type': 'application/json'
}


def _send_update_request(data):
    r = requests.post(
        _AUTOPILOT_API_UPDATE,
        json.dumps(data),
        headers=_headers)


def _send_trigger_request(trigger_id, user_email):
    r = requests.post(
        _AUTOPILOT_API_TRIGGER % (trigger_id, user_email),
        {},
        headers=_headers
    )


class AutopilotAPI:

    @staticmethod
    def update_user(first_name, last_name, email):
        data = {
            "contact": {
                "FirstName": first_name,
                "LastName": last_name,
                "Email": email,
                "custom": {
                    "string--DomainName": _AUTOPILOT_BACK_URL,
                }
            }
        }
        _send_update_request(data)

    @staticmethod
    def register_user(user_first_name, user_last_name, user_email):
        data = _DEFAULT_DATA.copy()
        data["contact"]["Email"] = user_email
        data["contact"]["FirstName"] = user_first_name
        data["contact"]["LastName"] = user_last_name
        data["contact"]["custom"]["string--DomainName"] = _AUTOPILOT_BACK_URL
        _send_update_request(data)
        _send_trigger_request(_TriggerKeys.REGISTER.value, user_email)

    @staticmethod
    def forgot_password(code, user_email):
        data = {
            "contact": {
                "Email": user_email,
                "custom": {
                    "string--ForgotPasswordCode": code,
                    "string--DomainName": _AUTOPILOT_BACK_URL,
                }
            }
        }
        _send_update_request(data)
        _send_trigger_request(_TriggerKeys.FORGOT_PASSWORD.value, user_email)

    @staticmethod
    def activate_subscription(date, price, user_email):
        data = {
            "contact": {
                "Email": user_email,
                "custom": {
                    "float--Price": price,
                    "string--PaymentDate": date,
                    "string--DomainName": _AUTOPILOT_BACK_URL,
                }
            }
        }
        _send_update_request(data)
        _send_trigger_request(_TriggerKeys.ACTIVATION.value, user_email)

    @staticmethod
    def cancel_subscription(user_email):
        data = {
            "contact": {
                "Email": user_email,
                "custom": {
                    "string--DomainName": _AUTOPILOT_BACK_URL,
                }
            }
        }
        _send_update_request(data)
        _send_trigger_request(_TriggerKeys.CANCELED.value, user_email)

    @staticmethod
    def billing_error(user_email):
        data = {
            "contact": {
                "Email": user_email,
                "custom": {
                    "string--DomainName": _AUTOPILOT_BACK_URL,
                }
            }
        }
        _send_update_request(data)
        _send_trigger_request(_TriggerKeys.BILLING_ERROR.value, user_email)

    @staticmethod
    def trial_expired(user_email):
        data = {
            "contact": {
                "Email": user_email,
                "custom": {
                    "string--DomainName": _AUTOPILOT_BACK_URL,
                }
            }
        }
        _send_update_request(data)
        _send_trigger_request(_TriggerKeys.TRIAL_EXPIRED.value, user_email)
