from io import BytesIO
from os.path import join
import base64

import uuid
from django.db import models
from django.conf import settings
from django.utils.translation import gettext as _
from datetime import datetime
from django.contrib.auth.hashers import Argon2PasswordHasher
from django.utils.crypto import get_random_string
from django.db.models.signals import post_save
from django.dispatch import receiver
import jwt
from graphql_jwt.exceptions import GraphQLJWTError
import stripe
from django.core.files import File

from apps.taskqueues.utils import trigger_backend_task
from apps.projects_app.models import Project
from apps.users_app.async_tasks.models import RegisterUserAsyncDTO
from .payments import PaymentInfo
from .tasks import forgot_password, \
    activate_subscription, cancel_subscription, \
    user_updated

admin_email = getattr(settings, 'ADMIN_EMAIL')
restore_password_link = getattr(settings, 'RESTORE_PASSWORD_LINK')

jwt_settings = getattr(settings, 'JWT_SETTINGS')


def upload_to(instance, filename):
    """ Set path to upload images """
    return join("photos/users/%s" % instance.id, filename)


def get_or_create_plan(plans):
    for plan in plans:
        if plan.id == settings.CURRENT_PLAN_ID:
            return plan
    return stripe.Plan.create(
        amount=settings.DEFAULT_STRIPE_AMMOUNT,
        interval=settings.DEFAULT_STRIPE_INTERVAL,
        product={
            "name": settings.DEFAULT_STRIPE_PLAN_NAME,
        },
        currency=settings.DEFAULT_STRIPE_CURRENCY,
        trial_period_days=settings.DEFAULT_TRIAL_PERIOD_DAYS
    )


class CustomModelMixin:

    def update(self, **fields):
        for field_name, value in fields.items():
            setattr(self, field_name, value)
        return self


class StripeCustomer(models.Model):
    """ Stripe customer model """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    external_id = models.CharField(
        _("Stripe id"),
        unique=True,
        max_length=64,
        blank=True)
    email = models.EmailField(
        _("Email"),
        max_length=256,
        blank=False,
        unique=True)
    plan_id = models.CharField(_("Plan id"), max_length=64, blank=True)
    plan_name = models.CharField(_("Plan name"), max_length=64, blank=True)
    subscription_id = models.CharField(
        _("Subscription id"),
        max_length=64,
        blank=True,
        null=True)

    user = models.OneToOneField("User", on_delete=models.CASCADE)

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    def __str__(self):
        return self.external_id

    @classmethod
    def find_by_user(cls, user):
        return cls.objects.filter(user=user).first()

    @classmethod
    def find_by_external_id(cls, external_id):
        return cls.objects.filter(external_id=external_id).first()

    @property
    def uuid(self):
        return str(self.id)

    def cancel_subscription(self):
        subscription = stripe.Subscription.retrieve(self.subscription_id)
        subscription.billing = "send_invoice"
        subscription.days_until_due = 3
        subscription.cancel_at_period_end = True
        subscription.save()

        ext_customer = stripe.Customer.retrieve(self.external_id)
        old_source = ext_customer.sources.retrieve(ext_customer.default_source)
        ext_customer.description = "Updated at %s" % str(datetime.utcnow())
        old_source.delete()
        ext_customer.save()

        cancel_subscription(self.user.email, self.user.uuid)

    @classmethod
    def create_payment_token(
            cls, card_number, card_exp_month, card_exp_year, card_cvv):
        return stripe.Token.create(
            card={
                "number": card_number,
                "exp_month": card_exp_month,
                "exp_year": card_exp_year,
                "cvc": card_cvv
            },
        )

    @classmethod
    def get_pinfo_by_user(cls, user):
        info = PaymentInfo()
        if user.is_admin:
            info.set_admin()
            return info
        instance = cls.objects.filter(user=user).first()
        if instance:
            return instance.get_info()
        else:
            if not (
                    datetime.utcnow().timestamp() -
                    user.created_at.timestamp() <
                    settings.TRIAL_PERIOD_SECONDS
            ):
                info.deactivate()
            info.current_period_end = user.created_at.timestamp() + \
                settings.TRIAL_PERIOD_SECONDS
            return info

    def get_info(self):
        info = PaymentInfo()
        ext_customer = stripe.Customer.retrieve(self.external_id)

        if ext_customer:
            try:
                data = ext_customer.subscriptions.data
                status = data[0].status
                current_period_end = data[0].current_period_end
                payment_source = ext_customer.retrieve_source(
                    ext_customer.id, ext_customer.default_source
                )
                last4 = getattr(payment_source, 'last4', None)
                exp_month = getattr(payment_source, 'exp_month', None)
                exp_year = getattr(payment_source, 'exp_year', None)

                charges = stripe.Charge.list(customer=self.external_id)
                info.extract_charges(charges['data'])
            except Exception as e:
                info.deactivate()
            else:
                if status == 'active':
                    info.add_card(
                        last4,
                        ext_customer.email,
                        exp_month,
                        exp_year,
                        current_period_end
                    )

                elif status == 'trialing':
                    info.add_card(
                        last4,
                        ext_customer.email,
                        exp_month,
                        exp_year,
                        current_period_end
                    )
                    # info.current_period_end = \
                    #     self.user.created_at.timestamp() + \
                    #     settings.DEFAULT_TRIAL_PERIOD_SECONDS
                else:
                    info.current_period_end = current_period_end
                    info.deactivate()
            finally:
                return info

        else:
            raise Exception("Invalid stripe id")

    @classmethod
    def update_or_create(cls, **kwargs):
        user = kwargs.pop('user')
        payment_token = cls.create_payment_token(**kwargs)
        instance = cls.objects.filter(user=user).first()

        # Add new card to existing customer
        if instance:
            customer = stripe.Customer.retrieve(instance.external_id)
            if customer:
                customer.sources.create(source=payment_token)

                if customer.default_source:
                    old_card = customer.sources.retrieve(
                        customer.default_source)
                    old_card.delete()

                customer.default_source = payment_token.card.id
                customer.description = "Updated at %s" % str(datetime.utcnow())

                customer.save()
                created = False
                message = ""
            else:
                raise Exception("Invalid stripe id")
            """ Here we could update plan in future """
        else:
            plans = stripe.Plan.list(limit=4)
            plan = get_or_create_plan(plans)

            customer = stripe.Customer.create(
                description="Customer %s" % user.email,
                email=user.email,
                source=payment_token
            )

            timedelta = datetime.utcnow().timestamp() - \
                user.created_at.timestamp()

            if timedelta >= settings.DEFAULT_TRIAL_PERIOD_SECONDS:
                billing_cycle_anchor = datetime.utcnow().timestamp()
            else:
                billing_cycle_anchor = settings.DEFAULT_TRIAL_PERIOD_SECONDS +\
                    user.created_at.timestamp()

            subscription = stripe.Subscription.create(
                customer=customer.id,
                billing="charge_automatically",
                trial_period_days=settings.DEFAULT_TRIAL_PERIOD_DAYS,
                items=[
                    {
                        "plan": plan.id,
                    },
                ],
                prorate=False,
            )

            instance = cls.objects.create(
                external_id=customer.id,
                email=user.email,
                user=user,
                plan_id=plan.id,
                plan_name="",
                subscription_id=subscription.id
            )
            instance.save()
            created = True
            message = ""
            activate_subscription(
                datetime.utcnow().strftime("%d, %b %Y"),
                plan.amount / 100,
                instance.user.email,
                instance.user.uuid,
            )

        return instance, created, message


class UserProfile(models.Model):
    """ User profile """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    _photo_url = models.CharField(
        _("Photo url"),
        max_length=256,
        blank=True,
        null=True)
    name = models.CharField(_("Name"), max_length=256, blank=True)
    photo = models.ImageField(
        upload_to=upload_to,
        blank=True,
        null=True
    )
    time_spend = models.CharField(_("Time spend"), max_length=32, blank=True)
    book_type = models.CharField(
        _("Book_type"),
        max_length=32,
        blank=True,
        null=True
    )
    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    def __str__(self):
        return self.name

    @property
    def uuid(self):
        return str(self.id)

    def get_FL_names(self):
        first, *last = self.name.split(' ')
        return first if first else "user", last[0] if last else ""

    @property
    def photo_url(self):
        if self._photo_url:
            return self._photo_url
        elif self.photo:
            return self.photo.url
        else:
            return None


class User(models.Model):
    """ Custom user model """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(
        _("Email"),
        max_length=256,
        blank=False,
        unique=True)
    password = models.CharField(
        _("Password"),
        max_length=265,
        blank=False,
        editable=False)
    is_active = models.BooleanField(_("Is active?"), default=True)
    is_admin = models.BooleanField(_("Is admin?"), default=False)
    deleted_date = models.DateTimeField(
        _("Deleted date"), blank=True, null=True)
    created_by_google = models.BooleanField(
        _("Created by google account"), default=False)
    created_by_facebook = models.BooleanField(
        _("Created by facebook account"), default=False)

    profile = models.OneToOneField(
        "UserProfile",
        on_delete=models.CASCADE,
        blank=True,
        null=True)

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        app_label = 'users_app'

    _tokenize_fields = (
        'uuid', 'email', 'timestamp'
    )

    hasher = Argon2PasswordHasher()

    is_authenticated = True
    _token = None

    def set_image(self, source):
        self.profile.photo.delete()
        self.profile.photo_url = None
        self.profile.photo.save(source.name, source)

    @property
    def stripe_customer(self):
        return StripeCustomer.objects.filter(user=self).first()

    @property
    def uuid(self):
        return str(self.id)

    @property
    def timestamp(self):
        return self.created_at.timestamp()

    @property
    def tokenize_fields(self):
        payload = {}
        for field in self._tokenize_fields:
            payload[field] = getattr(self, field)
        payload['token_created'] = datetime.utcnow().timestamp()
        return payload

    def __str__(self):
        return str(self.id) + ' ' + self.email

    def hash_password(self, password):
        self.password = self.hasher.encode(password, get_random_string())

    def _check_hash(self, password):
        return self.hasher.verify(password, self.password)

    def _encode_token(self):
        return jwt.encode(
            self.tokenize_fields,
            jwt_settings['JWT_SECRET_KEY'],
            jwt_settings['JWT_ALGORITHM'],
        ).decode('utf-8')

    @property
    def token(self):
        return self._encode_token()

    @staticmethod
    def _decode_token(token, context=None):
        return jwt.decode(
            token,
            jwt_settings['JWT_SECRET_KEY'],
            jwt_settings['JWT_VERIFY'],
            options={
                'verify_exp': jwt_settings['JWT_VERIFY_EXPIRATION'],
            },
            leeway=jwt_settings['JWT_LEEWAY'],
            audience=jwt_settings['JWT_AUDIENCE'],
            issuer=jwt_settings['JWT_ISSUER'],
            algorithms=[jwt_settings['JWT_ALGORITHM']]
        )

    @classmethod
    def login(cls, login, password):
        user = cls.objects.filter(email=login).first()
        if user and user._check_hash(password):
            return user, user._encode_token()
        raise Exception("Invalid password or email")

    @classmethod
    def find_by_token(cls, token, context=None):
        try:
            payload = cls._decode_token(token, context)
            user = cls.objects.filter(
                email=payload['email'],
                id=payload['uuid']).first()
        except jwt.ExpiredSignature:
            raise GraphQLJWTError(_('Signature has expired'))
        except jwt.DecodeError:
            raise GraphQLJWTError(_('Error decoding signature'))
        except jwt.InvalidTokenError:
            raise GraphQLJWTError(_('Invalid token'))
        return user

    @classmethod
    def find_by_id(cls, user_id):
        return cls.objects.filter(id=user_id).first()

    def delete(self):
        self.deleted_date = datetime.utcnow()
        self.is_active = False
        self.profile.delete()
        self.save()

    def update(self, **kwargs):
        password = kwargs.pop('password', None)
        old_password = kwargs.pop('old_password', None)
        email = kwargs.pop('email', None)
        name = kwargs.pop('name', None)
        file = kwargs.pop('file', None)

        if password and old_password:
            if self._check_hash(old_password):
                self.hash_password(password)
            else:
                raise Exception("Old password does not match")
        if email:
            self.email = email

        if name:
            self.profile.name = name
            self.profile.save()

        if file:
            header, data = file.split(',')
            try:
                file_ext = header.split('data:image/')[1].split(';base64')[0]
            except TypeError as err:
                raise err
            file_data = base64.b64decode(data)
            self.profile.photo.delete()
            file_name = "%s_%s.%s" % (get_random_string(
                6), datetime.utcnow().timestamp(), file_ext)
            self.profile.photo.save(file_name, File(BytesIO(file_data)))
            self.profile._photo_url = None
            self.profile.save()

        first_name, last_name = self.profile.get_FL_names()
        user_updated(self.email, self.uuid, first_name,
                     last_name)

        return self


@receiver(post_save, sender=User)
def post_save(sender, instance, created, **kwargs):
    try:
        if created:
            user_email = instance.email
            user_uuid = instance.uuid
            instance.hash_password(instance.password)
            first_name, last_name = instance.profile.get_FL_names()

            dto = RegisterUserAsyncDTO(
                user_uuid, user_email, first_name, last_name
            )
            trigger_backend_task(
                url="/async/users/register_user_async",
                payload=dto.to_json_string())

            # Project.create_default_projects(user_uuid)
    except Exception as e:
        raise Exception("Couldn't save user, original exception: %s" % str(e))


class ConfirmCode(models.Model):
    """ Confirm code sended after changing password """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    activated = models.BooleanField(_("Is activated?"), default=False)
    body = models.CharField(_("Code body"), max_length=16, unique=True)
    user = models.ForeignKey("User", on_delete=models.CASCADE)

    created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    def __str__(self):
        return self.body

    @classmethod
    def create(cls, user):
        instance = cls.objects.create(
            activated=False,
            body=get_random_string(),
            user=user,
        )
        instance.save()

        forgot_password(instance.body, user.email)

        return instance
