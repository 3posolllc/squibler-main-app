from apps.users_app.models import StripeCustomer
from datetime import datetime
from django.conf import settings


def login_required(func):
    def wrapper(*args, **kwargs):
        user = args[1].context.user
        if not user.is_authenticated:
            raise Exception('Authentication credentials were not provided')
        return func(*args, **kwargs)
    return wrapper


def login_required_simple(func):
    def wrapper(*args, **kwargs):
        user = args[0].user
        if not user.is_authenticated:
            raise Exception('Authentication credentials were not provided')
        return func(*args, **kwargs)
    return wrapper


def payment_required(func):
    def wrapper(*args, **kwargs):
        user = args[1].context.user
        if user.is_admin:
            return func(*args, **kwargs)
        payment_info = StripeCustomer.get_pinfo_by_user(user)
        if payment_info.status == payment_info.get_statuses().INACTIVE.value:
            raise Exception("Payment required")
        else:
            return func(*args, **kwargs)
    return wrapper


def payment_check(func):
    def wrapper(*args, **kwargs):
        user = args[1].context.user
        kwargs.update({'payment_info': StripeCustomer.get_pinfo_by_user(user)})
        return func(*args, **kwargs)
    return wrapper
