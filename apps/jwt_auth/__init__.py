from graphql_jwt.utils import get_authorization_header
from apps.users_app.models import User


class JWTAuthBackend:

    def authenticate(self, request=None, **credentials):
        if request is None:
            return None

        token = get_authorization_header(request)

        if token is not None:
            try:
                user = User.find_by_token(token)
                user._token = token
                return user
            except Exception as e:
                pass

        return None

    def get_user(self, user_id):
        return User.find_by_id(user_id)
