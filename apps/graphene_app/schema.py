import graphene
from apps.users_app.schema import UserMutations, UserQueries
from apps.projects_app.schema import ProjectMutations, ProjectQueries
from apps.boards_app.schema import BoardQueries, BoardMutations
from apps.sub_sections_app.schema import SubSectionMutations
from apps.sections_app.schema import SectionMutations


class Mutations(
    UserMutations,
    ProjectMutations,
    BoardMutations,
    SectionMutations,
    SubSectionMutations,
    graphene.ObjectType,
):
    pass


class Query(
    UserQueries,
    ProjectQueries,
    BoardQueries,
    graphene.ObjectType
):
    pass


schema = graphene.Schema(query=Query, mutation=Mutations, types=[])
