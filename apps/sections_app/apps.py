from django.apps import AppConfig


class SectionsConfig(AppConfig):
    name = 'sections_app'
