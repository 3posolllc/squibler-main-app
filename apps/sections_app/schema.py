from .models import Section
import graphene
from apps.jwt_auth.decorators import login_required
from graphene_django.types import DjangoObjectType
from apps.boards_app.schema import BoardInput, BoardType, BoardOutputType
from apps.sub_sections_app.schema import (
    SubSectionInput,
    SubSectionType,
    SubSectionOutputType
)


class SectionType(DjangoObjectType):
    boards = graphene.List(BoardType)
    section_title = graphene.String(required=False)
    sub_sections = graphene.List(SubSectionType)
    uuid = graphene.String()
    created_at = graphene.String()
    updated_at = graphene.String()

    class Meta:
        model = Section
        interfaces = (graphene.relay.Node, )
        only_fields = (
            'uuid',
            'section_title',
            'title',
            'text',
            'summary',
            'created_at',
            'updated_at',
            'boards',
            'sub_sections',
            'ordering',
        )


class SectionInput(graphene.InputObjectType):
    uuid = graphene.String(required=False)
    section_title = graphene.String(required=False)
    title = graphene.String(required=False)
    summary = graphene.String(required=False)
    text = graphene.String(required=False)
    ordering = graphene.Int(required=False)

    sub_sections = graphene.InputField(
        graphene.List(SubSectionInput), required=False)
    boards = graphene.InputField(graphene.List(BoardInput), required=False)


class SectionOutputType(graphene.ObjectType):
    section = graphene.Field(SectionType)
    created = graphene.Boolean()
    sub_sections_output = graphene.List(SubSectionOutputType)
    boards_output = graphene.List(BoardOutputType)


class DeleteSection(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=True)

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    @login_required
    def mutate(_, info, uuid):
        try:
            user = info.context.user

            Section.objects.get(id=uuid, user_id=user.id).delete()
            return DeleteSection(ok=True, error=None)
        except Exception as e:
            return DeleteSection(ok=False, error=str(e))


class SectionMutations(graphene.ObjectType):
    delete_section = DeleteSection.Field()
