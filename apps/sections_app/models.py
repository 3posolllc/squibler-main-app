from django.db import models
from django.db.transaction import atomic
from django.utils.translation import gettext as _
from apps.sub_sections_app.models import SubSection
from apps.boards_app.models import Board
import uuid
from apps.base_app.types import SubSectionOutput, BoardOutput
from apps.base_app.mixins import OrderingMixin


class Section(OrderingMixin):
    """ Section model """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    section_title = models.CharField(
        _("Section Title"),
        max_length=256,
        blank=True,
        null=True
    )
    title = models.CharField(_("Title"), max_length=256, blank=True)
    summary = models.TextField(_("Summary"),)
    text = models.TextField(_("Text"),)

    project_id = models.CharField(
        blank=True,
        editable=True,
        null=True,
        max_length=256)
    user_id = models.UUIDField(
        blank=False,
        editable=False,
        null=False,
        default='00000000-0000-0000-0000-000000000000')

    _created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    _updated_at = models.DateTimeField(_("Updated at"), auto_now=True)
    ordering = models.PositiveIntegerField()

    order_field_name = 'ordering'

    class Meta:
        verbose_name = _('section')
        verbose_name_plural = _('sections')
        app_label = 'sections_app'
        ordering = ('ordering',)

    @property
    def sub_sections(self):
        return SubSection.objects.filter(section_id=self.id)

    @property
    def boards(self):
        return Board.objects.filter(section_id=self.id)

    @property
    def uuid(self):
        return str(self.id)

    @property
    def created_at(self):
        return str(self._created_at)

    @property
    def updated_at(self):
        return str(self._updated_at)

    @classmethod
    def update_or_create(cls, **kwargs):

        sub_sections_json = kwargs.pop('sub_sections', [])
        boards_json = kwargs.pop('boards', [])
        project_id = kwargs.pop('project_id')
        user_id = kwargs.pop('user_id')
        ordering = kwargs.pop('ordering', -1)

        queryset = cls.objects.filter(
            user_id=user_id,
            project_id=project_id
        )

        if kwargs.get('uuid'):
            section = queryset.filter(id=kwargs.pop('uuid'))
            section.update(**kwargs)
            section = section.first()
            if ordering != -1:
                section.to(ordering, qs=queryset)
            created = False
        else:
            if ordering == -1:
                kwargs['ordering'] = queryset.count()
            else:
                kwargs['ordering'] = ordering
            section = cls.objects.create(
                **kwargs, project_id=project_id, user_id=user_id)
            section.save()
            created = True

        sub_sections_out = [
            SubSectionOutput(
                *
                SubSection.update_or_create(
                    **ssj,
                    section_id=section.id,
                    user_id=user_id)) for ssj in sub_sections_json]

        boards_out = [
            BoardOutput(
                *
                Board.update_or_create(
                    **bj,
                    section_id=section.id,
                    project_id=project_id,
                    user_id=user_id)) for bj in boards_json]

        return section, created, sub_sections_out, boards_out

    def get_related(self):
        return self.__class__.objects\
            .filter(
                project_id=self.project_id,
                user_id=self.user_id
            ).order_by('ordering')

    @staticmethod
    @atomic
    def sort_from_start(sections):
        i = 0
        for section in sections:
            section.ordering = i
            i += 1
            section.save()

    def delete(self):
        self.sub_sections.delete()
        self.boards.delete()
        super(Section, self).delete()
        self.sort_from_start(
            self.get_related()
        )

    @classmethod
    def copy_many(cls, old_project_id, new_project_id):
        for section in cls.objects.filter(project_id=old_project_id):
            old_section_id = section.pk
            section.pk = None
            section.project_id = new_project_id
            section.save()

            SubSection.copy_many(
                old_section_id=old_section_id,
                new_section_id=section.pk)
            Board.copy_many(
                old_project_id=old_project_id,
                old_section_id=old_section_id,
                new_project_id=new_project_id,
                new_section_id=section.pk
            )
