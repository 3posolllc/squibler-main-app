from django.core.management.base import BaseCommand
from django.db import transaction
from apps.sections_app.models import Section

class Command(BaseCommand):
    help = 'Duplicate Section tile to Section_title column'

    def handle(self, *args, **options):
        sections = Section.objects.all()
        print("--------- Total Sections = %d ---------" % sections.count())
        print("--------- Change Started ---------")
        with transaction.atomic():
            for section in sections[30000:]:
                if not section.section_title:
                    print("--------- id = %d ---------" % section.pk)
                    Section.objects.filter(pk=section.pk).update(section_title=section.title)
        print("--------- Change Ended ---------")