from rest_framework import serializers
from apps.sub_sections_app.serializers import SubSectionSerializer
from apps.boards_app.serializers import BoardSerializer
from .models import Section


class SectionSerializer(serializers.ModelSerializer):

    sub_sections = SubSectionSerializer(read_only=True, many=True).data
    boards = BoardSerializer(read_only=True, many=True).data

    class Meta:
        model = Section
        fields = (
            'uuid',
            'title',
            'section_title',
            'summary',
            'text',
            'sub_sections',
            'boards',
            'created_at',
            'updated_at')
