from django.apps import AppConfig


class SectionsConfig(AppConfig):
    name = 'sub_sections_app'
