from django.db import models
from django.utils.translation import gettext as _
import uuid


class SubSection(models.Model):
    """ Sub section model """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(_("Title"), max_length=256, blank=True)
    text = models.TextField(_("Text"),)

    section_id = models.UUIDField(blank=True, editable=True, null=True)
    user_id = models.UUIDField(
        blank=False,
        editable=False,
        null=False,
        default='00000000-0000-0000-0000-000000000000')

    _created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    _updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _('sub section')
        verbose_name_plural = _('sub sections')
        app_label = 'sub_sections_app'
        ordering = ('_created_at',)

    @property
    def created_at(self):
        return str(self._created_at)

    @property
    def updated_at(self):
        return str(self._updated_at)

    @property
    def uuid(self):
        return str(self.id)

    @classmethod
    def update_or_create(cls, **kwargs):
        if kwargs.get('uuid'):
            sub_section = cls.objects.filter(
                section_id=kwargs.pop('section_id'),
                id=kwargs.pop('uuid'))
            sub_section.update(**kwargs)
            sub_section = sub_section.first()
            created = False
        else:
            sub_section = cls.objects.create(**kwargs)
            created = True

        return sub_section, created

    @classmethod
    def copy_many(cls, old_section_id, new_section_id):
        for sub_section in cls.objects.filter(section_id=old_section_id):
            old_sub_section_id = sub_section.pk
            sub_section.pk = None
            sub_section.section_id = new_section_id
            sub_section.save()
