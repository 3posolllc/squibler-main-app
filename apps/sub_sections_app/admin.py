from django.contrib import admin
from .models import SubSection


class SubSectionAdmin(admin.ModelAdmin):
    class Meta:
        model = SubSection


admin.site.register(SubSection, SubSectionAdmin)
