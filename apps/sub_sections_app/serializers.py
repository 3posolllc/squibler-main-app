from rest_framework import serializers
from .models import SubSection


class SubSectionSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubSection
        fields = ('uuid', 'title', 'text', 'created_at', 'updated_at')
