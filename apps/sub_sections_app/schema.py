from .models import SubSection
import graphene
from apps.jwt_auth.decorators import login_required
from graphene_django.types import DjangoObjectType


class SubSectionType(DjangoObjectType):
    uuid = graphene.String()
    created_at = graphene.String()
    updated_at = graphene.String()

    class Meta:
        model = SubSection
        interfaces = (graphene.relay.Node, )
        only_fields = ('uuid', 'title', 'text', 'created_at', 'updated_at')


class SubSectionInput(graphene.InputObjectType):
    uuid = graphene.String(required=False)
    title = graphene.String(required=False)
    text = graphene.String(required=False)


class SubSectionOutputType(graphene.ObjectType):
    sub_section = graphene.Field(SubSectionType)
    created = graphene.Boolean()


class DeleteSubSection(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=True)

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    @login_required
    def mutate(_, info, uuid):
        try:
            user = info.context.user

            SubSection.objects.get(id=uuid, user_id=user.id).delete()
            return DeleteSubSection(ok=True, error=None)
        except Exception as e:
            return DeleteSubSection(ok=False, error=str(e))


class SubSectionMutations(graphene.ObjectType):
    delete_sub_section = DeleteSubSection.Field()
