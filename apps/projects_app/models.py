import base64
import logging
from io import BytesIO
from os.path import join
from django.db import models
from django.conf import settings
from django.utils.crypto import get_random_string
from django.utils.translation import gettext as _
from django.core.files import File
import uuid
from datetime import datetime
from apps.sections_app.models import Section
from apps.sub_sections_app.models import SubSection
from apps.boards_app.models import Board, Note
from apps.base_app.types import SectionOutput, BoardOutput
from .helpers import BulkCreator


DOIS = settings.DEFAULT_OBJECT_ID_STARTSWITH


def uuid_str():
    return uuid.uuid4()


def upload_to(instance, filename):
    """ Set path to upload images """
    return join("photos/projects/%s_%s" %
                (instance.id, instance.user_id), filename)


class ProjectManager(models.Model):
    """ Object for switching between project versions """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    projects = models.ManyToManyField("Project")
    latest_version = models.IntegerField(default=0)

    user_id = models.UUIDField(
        blank=False,
        editable=False,
        null=False,
        default='00000000-0000-0000-0000-000000000000')

    _created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    _updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    @property
    def uuid(self):
        return str(self.id)

    @property
    def created_at(self):
        return str(self._created_at)

    @property
    def updated_at(self):
        return str(self._updated_at)

    @classmethod
    def get_by_project(cls, project):
        return cls.objects.filter(
            projects__id__exact=project.id,
            projects__user_id__exact=project.user_id).first()

    def add_version(self, base_version_uuid):
        new_project = self.projects.get(
            uuid=base_version_uuid).copy(
            with_manager=False)
        new_project.version = self.latest_version + 1
        new_project.save()
        self.projects.add(new_project)
        if self.latest_version >= settings.MAX_PROJECT_VERSION:
            for project in self.projects.all():
                project.version -= 1
                project.save()
            # remove oldest version
            self.projects.remove(
                self.projects.all().order_by('version').first())
        else:
            self.latest_version += 1

        self.save()

        return self.projects.filter(is_active=True)

    @classmethod
    def delete_version(cls, current_project):
        pm = cls.get_by_project(current_project)
        project = pm.projects.get(uuid=current_project.uuid)
        if pm.latest_version == project.version:
            pm.latest_version -= 1
        project.delete()
        pm.save()

    def get_version(self, version):
        return self.projects.filter(version=version, is_active=True).first()

    def get_latest(self):
        return self.projects.filter(
            version=self.latest_version, is_active=True).first()


class Project(models.Model):
    """ Project model """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    uuid = models.CharField(default=uuid_str, editable=True, max_length=256)
    title = models.CharField(_("Title"), max_length=256, blank=True)
    type = models.CharField(_("Type"), max_length=256, blank=True, null=True)
    summary = models.TextField(_("Summary"), blank=True)
    content = models.TextField(_("Content"), blank=True)
    is_active = models.BooleanField(_("Is active?"), default=True)
    deleted_date = models.DateTimeField(
        _("Deleted date"), blank=True, null=True)
    thumbnail_name = models.CharField(
        _("Thumbnal name"), max_length=256, blank=True)
    thumbnail_url = models.CharField(
        _("Thumbnal url"),
        max_length=2084,
        null=True,
        blank=True,
        default="/assets/images/new_project.png"
    )
    default_thumbnail_url = models.CharField(
        _("Default Thumbnal url"),
        max_length=2084,
        null=True,
        blank=True
    )
    thumbnail_source = models.ImageField(
        upload_to=upload_to,
        blank=True,
        null=True
    )
    color = models.CharField(_("Color"), max_length=9, blank=True)
    version = models.IntegerField(_("Version"), default=0)
    use_as_template = models.BooleanField(default=False)

    user_id = models.UUIDField(
        blank=False,
        editable=False,
        null=False,
        default='00000000-0000-0000-0000-000000000000')

    _created_at = models.DateTimeField(_("Created at"), auto_now_add=True)
    _updated_at = models.DateTimeField(_("Updated at"), auto_now=True)

    class Meta:
        verbose_name = _('project')
        verbose_name_plural = _('projects')
        app_label = 'projects_app'
        ordering = ('_created_at',)
        unique_together = (('user_id', 'uuid'), )

    def __str__(self):
        return "%s - %s" % (self.title, str(self.uuid))

    @property
    def sections(self):
        return Section.objects.filter(
            project_id=self.uuid, user_id=self.user_id).order_by('ordering')

    @property
    def sections_count(self):
        return Section.objects.filter(
            project_id=self.uuid, user_id=self.user_id).count()

    @property
    def sub_sections_count(self):
        return sum(
            [len(section.sub_sections) for section in Section.objects.filter(
                project_id=self.uuid, user_id=self.user_id)]
        )

    @property
    def thumbnail(self):
        if self.thumbnail_url:
            return self.thumbnail_url
        elif self.thumbnail_source:
            return self.thumbnail_source.url

    @property
    def boards(self):
        return Board.objects.filter(
            project_id=self.uuid, section_id=None, user_id=self.user_id)

    @property
    def notes_count(self):
        return Note.objects.filter(
            board__in=Board.objects.filter(
                project_id=self.uuid, user_id=self.user_id)
        ).count()

    @property
    def timestamp(self):
        return self.created_at.timestamp()

    @property
    def created_at(self):
        return str(self._created_at)

    @property
    def updated_at(self):
        return str(self._updated_at)

    @classmethod
    def update_or_create(cls, **kwargs):
        sections_json = kwargs.pop('sections', [])
        boards_json = kwargs.pop('boards', [])
        user_id = kwargs.pop('user_id')
        file = kwargs.pop('file', None)

        if kwargs.get('uuid'):
            project = cls.objects.filter(
                uuid=kwargs.pop('uuid'), user_id=user_id)
            project.update(**kwargs)
            project = project.first()

            if file:
                header, data = file.split(',')
                try:
                    file_ext = header.split(
                        'data:image/')[1].split(';base64')[0]
                except TypeError as err:
                    raise err
                file_data = base64.b64decode(data)
                project.thumbnail_source.delete()
                file_name = "%s_%s.%s" % (get_random_string(
                    6), datetime.utcnow().timestamp(), file_ext)
                project.thumbnail_source.save(
                    file_name, File(BytesIO(file_data)))
                project.thumbnail_url = None

            project.save()
            created = False
        else:
            project = cls.objects.create(**kwargs, user_id=user_id)
            project.save()
            pm = ProjectManager.objects.create(user_id=user_id)
            pm.projects.add(project)
            pm.save()
            created = True

        sections_out = [
            SectionOutput(
                *
                Section.update_or_create(
                    **sj,
                    project_id=project.uuid,
                    user_id=user_id)) for sj in sections_json]

        boards_out = [
            BoardOutput(
                *
                Board.update_or_create(
                    **bj,
                    project_id=project.uuid,
                    user_id=user_id)) for bj in boards_json]

        return project, created, sections_out, boards_out

    def delete(self):
        self.is_active = False
        self.save()

    def create_template(self):
        old_pk = self.pk
        old_uuid = self.uuid
        self.pk = None
        self.uuid = uuid.uuid4()
        self.use_as_template = True
        self.save()

        Board.copy_many(old_project_id=old_uuid, new_project_id=self.uuid)
        Section.copy_many(old_project_id=old_uuid, new_project_id=self.uuid)

        return self

    def copy(self, with_manager=False, new_user_id=None):
        old_pk = self.pk
        old_uuid = self.uuid
        self.pk = None
        self.uuid = uuid.uuid4()
        self.save()

        if with_manager:
            user_id = new_user_id if new_user_id else self.user_id
            pm = ProjectManager.objects.create(user_id=user_id)
            pm.projects.add(self)
            pm.save()

        Board.copy_many(old_project_id=old_uuid, new_project_id=self.uuid)
        Section.copy_many(old_project_id=old_uuid, new_project_id=self.uuid)

        return self

    def have_image(self):
        return True if self.thumbnail_name else False

    def get_image_name(self):
        return self.thumbnail_name

    def set_image(self, url, name):
        self.thumbnail_url = url
        self.thumbnail_name = name
        self.save()

    @property
    def related(self):
        return ProjectManager.get_by_project(
            project=self).projects.filter(is_active=True)

    def get_html(self):
        html = ""
        html += "<h1>%s</h1>%s" % (self.title, self.content)
        for section in self.sections:
            html += "<div>"
            html += "<h4>%s</h4>%s" % (section.title, section.text)
            html += "</div>"
        return html

    def get_pagination_html(self):
        html = ""
        html += "<h1>%s</h1>%s" % (self.title, self.add_pagination_content(self.content))
        for section in self.sections:
            html += self.get_pagination()
            html += "<div>"
            html += "<h4>%s</h4>%s" % (section.title, self.add_pagination_content(section.text))
            html += "</div>"
        return html

    @staticmethod
    def add_pagination_content(content):
        return content

    @staticmethod
    def get_pagination():
        return "<div style='page-break-after: always'></div>"

    @classmethod
    def create_default_project(
            cls, user_id, default_project_id, default_project_structure):
        sections_json = default_project_structure.pop('sections', [])
        boards_json = default_project_structure.pop('boards', [])

        project = cls.objects.create(
            **default_project_structure, user_id=user_id)
        project.save()
        pm = ProjectManager.objects.create(user_id=user_id)
        pm.projects.add(project)
        pm.save()

        sections_out = [
            SectionOutput(
                *
                Section.update_or_create(
                    **sj,
                    project_id=default_project_id,
                    user_id=user_id)) for sj in sections_json]

        boards_out = [
            BoardOutput(
                *
                Board.update_or_create(
                    **bj,
                    project_id=default_project_id,
                    user_id=user_id)) for bj in boards_json]

        project.uuid = default_project_id
        project.save()

        return project, True, sections_out, boards_out

    @classmethod
    def create_default_projects(cls, user_id, uuid):

        bulk_creator = BulkCreator(user_id, ProjectManager)

        projects = cls.objects.filter(
            uuid=uuid, use_as_template=True
        )
        projects_output = bulk_creator.save_many_projects(projects)

        sections = Section.objects.filter(project_id=uuid)
        section_ids = [section.uuid for section in sections]
        sections_output = bulk_creator.save_many_sections(
            sections, projects_output
        )

        boards = Board.objects.filter(project_id=uuid)
        boards_ids = [board.uuid for board in boards]
        boards_output = bulk_creator.save_many_boards(
            boards,
            projects_output,
            sections_output
        )

        sub_sections = SubSection.objects.filter(section_id__in=section_ids)
        bulk_creator.save_many_subsections(sub_sections, sections_output)

        notes = Note.objects.filter(board__id__in=boards_ids)
        bulk_creator.save_many_notes(notes, boards_output)

        return projects[0], True, sections_output, boards_output
