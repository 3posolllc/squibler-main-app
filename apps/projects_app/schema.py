import logging
from io import FileIO
from graphene_django.types import DjangoObjectType
import graphene
from graphene_file_upload.scalars import Upload
from .models import Project, ProjectManager
from .serializers import ProjectSerializer
from apps.jwt_auth.decorators import login_required, payment_required
from apps.sections_app.schema import (
    SectionType,
    SectionInput,
    SectionOutputType
)
from apps.boards_app.schema import BoardType, BoardInput, BoardOutputType
from apps.users_app.models import User
from apps.converter_app import convert
from apps.base_app.types import ProjectOutput


class ProjectInterface:
    sections = graphene.List(SectionType)
    boards = graphene.List(BoardType)
    uuid = graphene.ID()
    sections_count = graphene.Int()
    sub_sections_count = graphene.Int()
    notes_count = graphene.Int()
    created_at = graphene.String()
    updated_at = graphene.String()


class ProjectTemplateType(ProjectInterface, DjangoObjectType):
    thumbnail = graphene.String()

    class Meta:
        model = Project
        interfaces = (graphene.relay.Node, )
        only_fields = ('title', 'summary', 'type', 'content', 'created_at',
                       'updated_at', 'is_active', 'deleted_date',
                       'user', 'sections', 'boards','use_as_template', 'color', 'thumbnail')


class ProjectType(ProjectInterface, DjangoObjectType):
    thumbnail = graphene.String()

    class Meta:
        model = Project
        interfaces = (graphene.relay.Node, )
        only_fields = ('title', 'summary', 'type', 'content', 'created_at',
                       'updated_at', 'is_active', 'deleted_date',
                       'user', 'sections', 'boards', 'color','use_as_template', 'thumbnail')


class ProjectRelatedType(ProjectInterface, DjangoObjectType):
    related = graphene.List(ProjectType)
    thumbnail = graphene.String()

    class Meta:
        model = Project
        interfaces = (graphene.relay.Node, )
        only_fields = ('title', 'summary', 'content', 'created_at',
                       'updated_at', 'is_active', 'deleted_date',
                       'user', 'sections', 'boards', 'color', 'thumbnail')


class ProjectOutputType(graphene.ObjectType):
    project = graphene.Field(ProjectType)
    created = graphene.Boolean()
    boards_output = graphene.List(BoardOutputType)
    sections_output = graphene.List(SectionOutputType)


class CreateOrUpdateProject(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=False)
        title = graphene.String(required=False)
        summary = graphene.String(required=False)
        content = graphene.String(required=False)
        thumbnail_name = graphene.String(required=False)
        thumbnail_url = graphene.String(required=False)
        color = graphene.String(required=False)
        sections = graphene.List(SectionInput)
        boards = graphene.List(BoardInput)
        file = Upload(required=False)

    ok = graphene.Boolean()
    project_output = graphene.Field(ProjectOutputType)
    error = graphene.String()

    @staticmethod
    @login_required
    @payment_required
    def mutate(_, info, **project_input):

        try:
            user = info.context.user
            project_out = ProjectOutput(
                *
                Project.update_or_create(
                    **project_input,
                    user_id=user.uuid))

            return CreateOrUpdateProject(
                project_output=project_out,
                ok=True,
                error=None,
            )
        except Exception as e:
            logging.exception("Error in update or create")
            return CreateOrUpdateProject(
                ok=False,
                error=str(e),
            )


class CreateProjectFromTemplate(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=False)

    ok = graphene.Boolean()
    project_output = graphene.Field(ProjectOutputType)
    error = graphene.String()

    @staticmethod
    @login_required
    @payment_required
    def mutate(_, info, uuid):

        try:
            user = info.context.user

            projects_output, created, sections_output, boards_output = Project.create_default_projects(user.uuid, uuid)
            project_out = ProjectOutput(projects_output, created, sections_output, boards_output)

            return CreateProjectFromTemplate(
                project_output=project_out,
                ok=True,
                error=None,
            )
        except Exception as e:
            logging.exception("Error in update or create")
            return CreateProjectFromTemplate(
                ok=False,
                error=str(e),
            )


class DuplicateProject(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=True)

    ok = graphene.Boolean()
    project = graphene.Field(ProjectType)
    error = graphene.String()

    @staticmethod
    @login_required
    @payment_required
    def mutate(_, info, uuid):
        try:
            user = info.context.user
            new_project = Project.objects.get(
                uuid=uuid, user_id=user.id).copy(
                with_manager=True)

            return DuplicateProject(ok=True, error=None, project=new_project)
        except Exception as e:
            return DuplicateProject(ok=False, error=str(e))


class AddNewVersion(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=True)

    ok = graphene.Boolean()
    error = graphene.String()
    projects = graphene.List(ProjectType)

    @staticmethod
    @login_required
    @payment_required
    def mutate(_, info, uuid):
        try:
            user = info.context.user
            project = Project.objects.get(uuid=uuid)
            projects_manager = ProjectManager.get_by_project(project=project)
            projects = projects_manager.add_version(base_version_uuid=uuid)

            return AddNewVersion(ok=True, error=None, projects=projects)
        except Exception as e:
            return AddNewVersion(ok=False, error=str(e))


class DeleteProject(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=False)
        delete_all = graphene.Boolean(required=False)

    ok = graphene.Boolean()
    error = graphene.String()

    @staticmethod
    @login_required
    @payment_required
    def mutate(_, info, **kwargs):
        try:
            user = info.context.user
            if kwargs.get('delete_all', False):
                pm = ProjectManager.get_by_project(
                    Project.objects.get(
                        uuid=kwargs['uuid'],
                        user_id=user.id))
                for project in pm.projects.filter(is_active=True):
                    project.delete()
            else:
                project = Project.objects.get(
                    uuid=kwargs['uuid'], user_id=user.id)
                ProjectManager.delete_version(project)
            return DeleteProject(ok=True, error=None)
        except Exception as e:
            return DeleteProject(ok=False, error=str(e))


class File(graphene.Scalar):
    def serialize(self):
        pass


class ExportProject(graphene.Mutation):
    class Arguments:
        uuid = graphene.String(required=True)
        file_format = graphene.String(required=True)

    ok = graphene.Boolean()
    error = graphene.String()
    export = File()

    @staticmethod
    @login_required
    def mutate(_, info, uuid, file_format):
        try:
            user = info.context.user
            project = Project.objects.get(uuid=uuid, user_id=user.id)
            name = "".join(project.title.lower().split(" "))
            file_path = convert(name, project.get_html(), file_format)
            return ExportProject(ok=True, error=None, export=FileIO(file_path))
        except Exception as e:
            return ExportProject(ok=False, error=str(e))


class ProjectMutations(graphene.ObjectType):
    create_or_update_project = CreateOrUpdateProject.Field()
    duplicate_project = DuplicateProject.Field()
    delete_project = DeleteProject.Field()
    add_new_version = AddNewVersion.Field()
    export_project = ExportProject.Field()
    create_project_from_template = CreateProjectFromTemplate.Field()


class ProjectQueries(graphene.ObjectType):
    templates = graphene.List(ProjectTemplateType)
    projects = graphene.List(ProjectType)
    project = graphene.Field(
        ProjectRelatedType,
        uuid=graphene.String(
            required=True))

    @login_required
    def resolve_templates(self, info, **kwargs):
        return Project.objects.filter(use_as_template=True)

    @login_required
    def resolve_projects(self, info, **kwargs):
        user = info.context.user

        return [pm.get_latest() for pm in ProjectManager.objects.filter(
            user_id=user.id).order_by('-projects___updated_at') if pm.get_latest()]

    @login_required
    @payment_required
    def resolve_project(self, info, **kwargs):
        user = info.context.user

        return Project.objects.filter(
            uuid=kwargs['uuid'], user_id=user.id, is_active=True).first()
