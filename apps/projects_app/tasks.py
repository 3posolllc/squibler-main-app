from datetime import datetime

import logging
from django.conf import settings
import analytics

from apps.base_app.helpers import create_segment_data

analytics.write_key = settings.SEGMENT_API_KEY
ENVIRONMENT_NAME = settings.ENVIRONMENT_NAME

logger = logging.getLogger(__name__)


def project_created(user_uuid, project_id, google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            projectId=project_id,
        )

        analytics.track(user_uuid, 'Project created', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Project created error")


def project_updated(user_uuid, project_id,
                    google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            projectId=project_id,
        )
        analytics.track(user_uuid, 'Project updated', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Project updated error")


def project_duplicated(user_uuid, project_id, new_project_id,
                       google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            projectId=project_id,
            newProjectId=new_project_id,
        )
        analytics.track(user_uuid, 'Project duplicated', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Project duplicated error")


def project_deleted(user_uuid, project_id, google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            projectId=project_id,
        )
        analytics.track(user_uuid, 'Project deleted', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Project deleted")


def project_new_version(user_uuid, project_id,
                        google_analytics_id=None):
    try:
        data, integrations = create_segment_data(
            user_uuid,
            google_analytics_id=google_analytics_id,
            projectId=project_id,
        )
        analytics.track(user_uuid, 'New project version', data,
                        integrations=integrations
                        )
    except BaseException:
        import logging
        logging.exception("Project new version")
