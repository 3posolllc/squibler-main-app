from django.db.transaction import atomic
import uuid

class BulkCreator:

    def __init__(self, user_id, project_manager):
        self.user_id = user_id
        self.project_manager = project_manager

    class ZeroModel:
        pk = None

    @atomic
    def save_many_projects(self, projects):
        output = {}
        for project in projects:
            project_sections = project.sections
            project_boards = project.boards
            old_project_id = "{}:{}".format(project.uuid, project.user_id)
            project.pk = None
            project.user_id = self.user_id
            project.uuid = uuid.uuid4()
            project.thumbnail_url = project.default_thumbnail_url
            project.use_as_template = False
            project.save()
            output[old_project_id] = project

            pm = self.project_manager.objects.create(
                user_id=self.user_id
            )
            pm.projects.add(project)
            pm.save()

        return output

    @atomic
    def save_many_sections(self, sections, projects_output):
        sections_output = {}
        for section in sections[:]:

            old_section_id = section.pk
            old_project_id = "{}:{}".format(
                section.project_id,
                section.user_id
            )

            section.pk = None
            section.user_id = self.user_id
            section.project_id = projects_output[old_project_id].uuid if old_project_id in projects_output else None
            section.save()

            sections_output[old_section_id] = section

        sections_output[None] = self.ZeroModel
        return sections_output

    @atomic
    def save_many_boards(self, boards, projects_output, sections_output):
        boards_output = {}
        for board in boards[:]:

            old_board_id = board.pk
            old_project_id = "{}:{}".format(board.project_id, board.user_id)
            old_section_id = board.section_id

            board.pk = None
            board.user_id = self.user_id
            board.section_id = sections_output[old_section_id].pk if old_section_id in sections_output else None
            board.project_id = projects_output[old_project_id].uuid
            board.save()

            boards_output[old_board_id] = board

        return boards_output

    @atomic
    def save_many_notes(self, notes, boards_output):
        for note in notes:
            old_note_id = note.pk
            old_board_id = note.board_id

            note.pk = None
            note.board_id = boards_output[old_board_id].pk
            note.save()

    @atomic
    def save_many_subsections(self, sub_sections, sections_output):
        for sub_section in sub_sections:
            old_sub_section_id = sub_section.pk
            old_section_id = sub_section.section_id

            sub_section.pk = None
            sub_section.section_id = sections_output[old_section_id].pk
            sub_section.user_id = self.user_id
            sub_section.save()
