from django.core.management.base import BaseCommand
from apps.projects_app.models import Project
from apps.users_app.models import User
import logging

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Create default Peoject Template from existing Project'

    def add_arguments(self, parser):
        parser.add_argument('user_email', type=str, help='Target user email')
        parser.add_argument('project_uuid', type=str, help='Target project uuid')

    def handle(self, *args, **options):
        project_uuid = options['project_uuid']
        user_email = options['user_email']
        user = User.objects.filter(email=user_email).first()

        if not user:
            logger.error("--- User does not exist ---")
            return False

        project = Project.objects.filter(uuid=project_uuid, user_id=user.uuid)\
            .first()

        if not project:
            logger.error("--- Project does not exist ---")
            return False

        logger.debug("Creating Template...")

        template = project.create_template()
