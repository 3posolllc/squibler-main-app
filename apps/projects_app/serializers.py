from rest_framework import serializers
from apps.sections_app.serializers import SectionSerializer
from apps.boards_app.serializers import BoardSerializer
from .models import Project


class ProjectSerializer(serializers.ModelSerializer):

    sections = SectionSerializer(many=True).data
    boards = BoardSerializer(many=True).data

    class Meta:
        model = Project
        fields = (
            'uuid',
            'title',
            'summary',
            'sections',
            'boards',
            'created_at',
            'updated_at',
            'thumbnail',
            'color',
        )
