from flask import Flask, request, redirect
from urlparse import urlparse, urlunparse
import logging

app = Flask(__name__)


@app.before_request
def redirect_www():
    """Redirect www requests to non-www."""
    urlparts = urlparse(request.url)
    logging.info(urlparts)
    if not urlparts.netloc.startswith("www"):
        urlparts_list = list(urlparts)
        domain_name = urlparts_list[1]
        urlparts_list[1] = ".".join(["www", domain_name])
        logging.info("after change")
        logging.info(urlparts_list)
        return redirect(urlunparse(urlparts_list), code=301)


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return "You have been redirected"