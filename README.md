# Squibler-Main-App

------Gcloud configuration
https://cloud.google.com/sdk/docs/#install_the_latest_cloud_sdk_version
* install the gcloud sdk
* initialize the tool with command `gcloud init`
* configure tool and authorize yourself with command `gcloud auth login`

------It the First terminal window
cd squibler-main-app
source env/bin/activate

For Linux machines use:
./cloud_sql_proxy -instances="sqiblify:us-west1:sqiblify-development-db"=tcp:5432

For MacOs machines use:
./cloud_sql_proxy_mac -instances="sqiblify:us-west1:sqiblify-development-db"=tcp:5432

./cloud_sql_proxy_mac -instances="sqiblify:us-west1:sqiblify-development-db"=tcp:5432

For window machines use:
cloud_sql_proxy_[x64 | x86].exe -instances="sqiblify:us-west1:sqiblify-development-db"=tcp:5432


source env/bin/activate
git checkout [the branch you want to set live]
git pull origin [the branch you want to set live]


------Back on cloud terminal
------(If you made changes to requirements)
pip install -r requirements.txt

------(Updating Cloud Tasks Queues)
Enable Cloud Tasks API in GCP console by typying it in search 
or finding in API library or marketplace
When you make any changes to queue.yaml to update queues:

gcloud app deploy queue.yaml

------(If you made changes to the database)
python manage.py makemigrations --dry-run
python manage.py makemigrations
python manage.py migrate

gcloud app deploy --version=[NewVersion#] --no-promote --no-stop-previous-version
gcloud app deploy --version=12 --no-promote --no-stop-previous-version
 
--version -> number of version of application to deploy
--no-promote -> after deployment, the version will not be promoted to default version of AppEngine
--no-stop-previous-version -> after deployment, previous default version will not be stopped
