from google.cloud import runtimeconfig
import os
import logging


logger = logging.getLogger(__name__)
DEV_APP_ID = "sqiblify"


def on_production():
    return 'SERVER_SOFTWARE' in os.environ and not os.environ['SERVER_SOFTWARE'].startswith(
        "Development")


def is_development_server():
    return not on_production()


def mock_get_application_id():
    try:
        config = runtimeconfig.Client()
        application_id = config.project
    except Exception:
        logger.error("Error at getting application id.")
        application_id = DEV_APP_ID

    return application_id


def get_application_version():
    try:
        client = runtimeconfig.Client()
        config = client.config(mock_get_application_id())
        return config.get_variable("version")
    except Exception:
        logger.error("Error at getting application version")

    return None
