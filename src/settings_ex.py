"""
Django settings for src project.

Generated by 'django-admin startproject' using Django 2.1.1.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

from google.oauth2 import service_account
import os
from datetime import timedelta
from os import environ as env
from project_default_examples import (
    DEFAULT_PROJECT_STRUCTURE as DPS1,
    DEFAULT_PROJECT_2_STRUCTURE as DPS2
)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
ROOT_DIR = os.path.dirname(
    os.path.dirname(
        os.path.dirname(
            os.path.abspath(__file__))))

ENVIRONMENT_NAME = "local"

INSTALLED_APPS = [
    # Default
    'djamin',  # third party
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_filters',
    # Third party
    'corsheaders',
    'graphene',
    'graphene_django',
    # Custom
    'apps.base_app',
    'apps.payments_app',
    'apps.sub_sections_app',
    'apps.boards_app',
    'apps.sections_app',
    'apps.projects_app',
    'apps.users_app',
    'apps.graphene_app',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'graphql_jwt.middleware.JSONWebTokenMiddleware',  # third party
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

AUTHENTICATION_BACKENDS = [
    'apps.jwt_auth.JWTAuthBackend',  # custom
    'django.contrib.auth.backends.ModelBackend',
]

ROOT_URLCONF = 'src.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'src.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASE_ROUTERS = ('apps.base_app.database_router.DefaultRouter', )


DATABASE_APPS_MAPPING = {
    # Built-in
    'contenttypes': 'default',
    'auth': 'default',
    'admin': 'default',
    'sessions': 'default',
    'messages': 'default',
    'staticfiles': 'default',
    # Custom
    'users_app': 'users_db',
    'projects_app': 'projects_db',
    'sections_app': 'sections_db',
    'sub_sections_app': 'sub_sections_db',
    'boards_app': 'boards_db',
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = "EN_us"

TIME_ZONE = "UTC"

USE_I18N = False

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(ROOT_DIR, 'static')

# GraphQL schema
GRAPHENE = {
    'SCHEMA': 'apps.graphene_app.schema.schema'
}


# JWT settings
JWT_SETTINGS = {
    'JWT_ALGORITHM': 'HS256',
    'JWT_AUDIENCE': None,
    # 'JWT_AUTH_HEADER': 'HTTP_AUTHORIZATION',
    'JWT_AUTH_HEADER_PREFIX': 'JWT',
    'JWT_ISSUER': None,
    'JWT_LEEWAY': 0,
    'JWT_SECRET_KEY': 'bllwu*#3q^ca3nmoxx+vx+&89$zri05xpo6&2u7i!pwssbfq5r',
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': False,
    'JWT_EXPIRATION_DELTA': timedelta(seconds=60 * 5),
    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': timedelta(days=7),
}

PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.Argon2PasswordHasher',
)

# CORS settings
CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_METHODS = (
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'file': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'formatter': 'verbose',
            'filename': 'logger_output.txt'
        },
    },
    'loggers': {
        'celery': {
            'handlers': ['console', 'file'],
            'level': 'INFO',
            'propagate': False,
        },
    }
}

TRIAL_PERIOD_SECONDS = 864000
DEFAULT_STRIPE_AMMOUNT = 1000
DEFAULT_STRIPE_INTERVAL = "month"
DEFAULT_STRIPE_PLAN_NAME = "Base plan"
DEFAULT_STRIPE_CURRENCY = "usd"

# Export settings
PATH_TO_SAVED_FILES = os.path.join(STATIC_ROOT, 'converted_output')

MAX_PROJECT_VERSION = 3

DEFAULT_PROJECT_ID = "fiction-demo-project"
DEFAULT_PROJECT_2_ID = "non-fiction-demo-project"

DEFAULT_PROJECT_STRUCTURE = DPS1
DEFAULT_PROJECT_2_STRUCTURE = DPS2

DATA_UPLOAD_MAX_MEMORY_SIZE = 10485760

FACEBOOK_API_URL = "https://graph.facebook.com/v2.9/me?access_token={}&fields=name%2Cemail%2Cpicture%2Cfirst_name%2Clast_name&method=get&pretty=0&sdk=joey&suppress_http_code=1"

GOOGLE_API_URL = "https://oauth2.googleapis.com/tokeninfo?id_token={}"

SECRET_KEY = '$#4glu1uros#9-!tw+b1-+-yyy=0%!iqv$&nj@ic&e$(9su-#$'

DEBUG = True

ALLOWED_HOSTS = ("www,squibler.io", "dev.squibler.io", "0.0.0.0")


DATABASES = {
    "boards_db": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": env.get("DB_BOARDS_HOST", "0.0.0.0"),
        "NAME": env.get("DB_BOARDS_NAME", "boards_db"),
        "PASSWORD": env.get("DB_BOARDS_PASSWORD", "password"),
        "PORT": env.get("DB_BOARDS_PORT", "5438"),
        "USER": env.get("DB_BOARDS_USER", "user")
    },
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": env.get("DB_DEFAULT_HOST", "0.0.0.0"),
        "NAME": env.get("DB_DEFAULT_NAME", "default_db"),
        "PASSWORD": env.get("DB_DEFAULT_PASSWORD", "password"),
        "PORT": env.get("DB_DEFAULT_PORT", "5433"),
        "USER": env.get("DB_DEFAULT_USER", "user")
    },
    "projects_db": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": env.get("DB_PROJECTS_HOST", "0.0.0.0"),
        "NAME": env.get("DB_PROJECTS_NAME", "projects_db"),
        "PASSWORD": env.get("DB_PROJECTS_PASSWORD", "password"),
        "PORT": env.get("DB_PROJECTS_PORT", "5435"),
        "USER": env.get("DB_PROJECTS_USER", "user")
    },
    "sections_db": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": env.get("DB_SECTIONS_HOST", "0.0.0.0"),
        "NAME": env.get("DB_SECTIONS_NAME", "sections_db"),
        "PASSWORD": env.get("DB_SECTIONS_PASSWORD", "password"),
        "PORT": env.get("DB_SECTIONS_PORT", "5436"),
        "USER": env.get("DB_SECTIONS_USER", "user")
    },
    "sub_sections_db": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": env.get("DB_SUBSECTIONS_HOST", "0.0.0.0"),
        "NAME": env.get("DB_SUBSECTIONS_NAME", "sub_sections_db"),
        "PASSWORD": env.get("DB_SUBSECTIONS_PASSWORD", "password"),
        "PORT": env.get("DB_SUBSECTIONS_PORT", "5437"),
        "USER": env.get("DB_SUBSECTIONS_USER", "user")
    },
    "users_db": {
        "ENGINE": "django.db.backends.postgresql",
        "HOST": env.get("DB_USERS_HOST", "0.0.0.0"),
        "NAME": env.get("DB_USERS_NAME", "users_db"),
        "PASSWORD": env.get("DB_USERS_PASSWORD", "password"),
        "PORT": env.get("DB_USERS_PORT", "5434"),
        "USER": env.get("DB_USERS_USER", "user")
    }
}

RESTORE_PASSWORD_LINK = "https://dev.squibler.io/forgot-password?confirm_code="

CORS_ORIGIN_WHITELIST = (
    "dev.squibler.io",
)

# SMTP settings
ADMIN_EMAIL = env.get("ADMIN_EMAIL")
EMAIL_HOST = env.get("EMAIL_HOST")
EMAIL_PORT = env.get("EMAIL_PORT")
EMAIL_HOST_USER = env.get("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = env.get("EMAIL_HOST_PASSWORD")
EMAIL_USE_TLS = False
EMAIL_USE_SSL = True
EMAIL_TIMEOUT = 30
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

# Google cloud storage settings
DEFAULT_FILE_STORAGE = 'storages.backends.gcloud.GoogleCloudStorage'
GS_BUCKET_NAME = 'squibler_bucket'


GS_CREDENTIALS = service_account.Credentials.from_service_account_file(
    "src/google_keys/sqiblify-08f18b0cbb5a.json"
)
GS_DEFAULT_ACL = 'publicRead'

# Stripe settings
STRIPE_API_KEY = env.get("STRIPE_API_KEY")
CURRENT_PLAN_ID = env.get("CURRENT_PLAN_ID")

AUTOPILOT_API_KEY = "9c3f6dafd3ef46bba819897f3ec6c868"
AUTOPILOT_API_UPDATE = "https://api2.autopilothq.com/v1/contact"
# % (trigger_id, user_email)
AUTOPILOT_API_TRIGGER = "https://api2.autopilothq.com/v1/trigger/%s/contact/%s"
AUTOPILOT_BACK_URL = "http://dev.squibler.io"

SEGMENT_API_KEY = "2e75niFI0wCFJYBaJgh4lSW2jHSQtZfw"
