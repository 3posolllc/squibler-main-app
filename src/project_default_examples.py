
DEFAULT_PROJECT_STRUCTURE = {'title': "The Lord of the Rings",
                             'summary': "My first project",
                             'content': """
        <h2>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</h2>

        <p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
        Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
        Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
        Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
        Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
        Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
        Sed eu ultricies massa</p>

        <p>In pretium turpis sed urna ullamcorper, ac gravida nulla mattis.
        Vestibulum pellentesque pellentesque purus ac blandit. Pellentesque quis quam lacus.
        Nulla dapibus aliquam nibh, eget tempor justo laoreet quis. Proin viverra suscipit vehicula.
        Maecenas pharetra odio nisl, at imperdiet quam consectetur in. Nam rutrum tempor viverra.
        Nulla consectetur urna sit amet elit gravida tempus. Nam consequat mi sollicitudin massa dictum,
        quis mollis augue venenatis. Fusce vel sem et tortor cursus aliquet. Praesent a feugiat est, sed condimentum nisi.
        Mauris cursus sapien eu sapien pretium, eu malesuada nisl luctus. Nullam ut convallis sem.
        Nunc magna dolor, varius vitae mattis in, viverra quis enim. Nulla aliquam bibendum ipsum ac lacinia.
        Mauris convallis tortor elit.</p>
    """,
                             'thumbnail_name': "",
                             'thumbnail_url': "https://20ui41tp7v127j03rcnp97oh-wpengine.netdna-ssl.com/wp-content/uploads/2017/01/two-towers_mumford.jpg",
                             'color': "",
                             'boards': [{'name': "Notes",
                                         'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                         'notes': [{'title': "Notes about Characters",
                                                    "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                    },
                                                   {'title': "Notes about Locations",
                                                    "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                    }],
                                         'icon_name': "notepad",
                                         'icon_url': "",
                                         'color': "",
                                         },
                                        {'name': "Research",
                                         'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                         'notes': [{'title': "Character Research",
                                                    "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                    },
                                                   {'title': "Location Research",
                                                    'text': """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                    }],
                                         'icon_name': "pen",
                                         'icon_url': "",
                                         'color': "",
                                         },
                                        {'name': "Scene Ideas",
                                         'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                         'notes': [{'title': "Opening Location",
                                                    "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                    },
                                                   {'title': "Closing Location",
                                                    "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                    }],
                                         'icon_name': "pin",
                                         'icon_url': "",
                                         'color': "",
                                         }],
                             'sections': [{'title': "Chapter 1",
                                           'ordering': 0,
                                           'summary': """Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna""",
                                           'text': """<p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
            Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
            Sed eu ultricies massa</p>""",
                                           'boards': [{'name': "Opening Scene Research",
                                                       'summary': "",
                                                       'notes': [{'title': "Location Ideas",
                                                                  "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                                  }],
                                                       'icon_name': "notepad",
                                                       'icon_url': "",
                                                       'color': "",
                                                       }],
                                           'sub_sections': [{'title': "Scene 1",
                                                             'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."},
                                                            {'title': "Scene 2",
                                                             'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."}],
                                           },
                                          {'title': "Chapter 2",
                                           'ordering': 1,
                                           'summary': """Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna""",
                                           'text': """<p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
            Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
            Sed eu ultricies massa</p>""",
                                           'boards': [{'name': "Opening Scene Research",
                                                       'summary': "",
                                                       'notes': [{'title': "Location Ideas",
                                                                  "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                                  }],
                                                       'icon_name': "notepad",
                                                       'icon_url': "",
                                                       'color': "",
                                                       }],
                                           'sub_sections': [{'title': "Scene 1",
                                                             'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."},
                                                            {'title': "Scene 2",
                                                             'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."}],
                                           },
                                          {'title': "Chapter 3",
                                           'ordering': 2,
                                           'summary': """Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna""",
                                           'text': """<p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
            Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
            Sed eu ultricies massa</p>""",
                                           'boards': [{'name': "Opening Scene Research",
                                                       'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                                       'notes': [{'title': "Location Ideas",
                                                                  "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                                  }],
                                                       'icon_name': "notepad",
                                                       'icon_url': "",
                                                       'color': "",
                                                       }],
                                           'sub_sections': [{'title': "Scene 1",
                                                             'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."},
                                                            {'title': "Scene 2",
                                                             'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."}],
                                           },
                                          {'title': "Chapter 4",
                                           'ordering': 3,
                                           'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                           'text': """<p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
            Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
            Sed eu ultricies massa</p>""",
                                           'boards': [{'name': "Opening Scene Research",
                                                       'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                                       'notes': [{'title': "Location Ideas",
                                                                  "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                                  }],
                                                       'icon_name': "notepad",
                                                       'icon_url': "",
                                                       'color': "",
                                                       }],
                                           'sub_sections': [{'title': "Scene 1",
                                                             'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."},
                                                            {'title': "Scene 2",
                                                             'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."}],
                                           }],
                             }

DEFAULT_PROJECT_2_STRUCTURE = {'title': "Seven Habits of Highly Effective People",
                               'summary': "My second project",
                               'content': """
        <h2>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...</h2>

        <p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
        Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
        Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
        Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
        Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
        Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
        Sed eu ultricies massa</p>

        <p>In pretium turpis sed urna ullamcorper, ac gravida nulla mattis.
        Vestibulum pellentesque pellentesque purus ac blandit. Pellentesque quis quam lacus.
        Nulla dapibus aliquam nibh, eget tempor justo laoreet quis. Proin viverra suscipit vehicula.
        Maecenas pharetra odio nisl, at imperdiet quam consectetur in. Nam rutrum tempor viverra.
        Nulla consectetur urna sit amet elit gravida tempus. Nam consequat mi sollicitudin massa dictum,
        quis mollis augue venenatis. Fusce vel sem et tortor cursus aliquet. Praesent a feugiat est, sed condimentum nisi.
        Mauris cursus sapien eu sapien pretium, eu malesuada nisl luctus. Nullam ut convallis sem.
        Nunc magna dolor, varius vitae mattis in, viverra quis enim. Nulla aliquam bibendum ipsum ac lacinia.
        Mauris convallis tortor elit.</p>
    """,
                               'thumbnail_name': "",
                               'thumbnail_url': "https://images-na.ssl-images-amazon.com/images/I/51hV5vGr4AL.jpg",
                               'color': "",
                               'boards': [{'name': "Notes",
                                           'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                           'notes': [{'title': "Notes about Characters",
                                                      "text": "",
                                                      },
                                                     {'title': "Notes about Locations",
                                                      "text": "",
                                                      }],
                                           'icon_name': "notepad",
                                           'icon_url': "",
                                           'color': "",
                                           },
                                          {'name': "Research",
                                           'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                           'notes': [{'title': "Character Research",
                                                      "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                      },
                                                     {'title': "Location Research",
                                                      'text': """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                      }],
                                           'icon_name': "pen",
                                           'icon_url': "",
                                           'color': "",
                                           },
                                          {'name': "Scene Ideas",
                                           'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                           'notes': [{'title': "Opening Location",
                                                      "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                      },
                                                     {'title': "Closing Location",
                                                      "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                      }],
                                           'icon_name': "pin",
                                           'icon_url': "",
                                           'color': "",
                                           }],
                               'sections': [{'title': "Chapter 1",
                                             'ordering': 0,
                                             'summary': """Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna""",
                                             'text': """<p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
            Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
            Sed eu ultricies massa</p>""",
                                             'boards': [{'name': "Opening Scene Research",
                                                         'summary': "",
                                                         'notes': [{'title': "Location Ideas",
                                                                    "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                                    }],
                                                         'icon_name': "notepad",
                                                         'icon_url': "",
                                                         'color': "",
                                                         }],
                                             'sub_sections': [{'title': "Scene 1",
                                                               'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."},
                                                              {'title': "Scene 2",
                                                               'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."}],
                                             },
                                            {'title': "Chapter 2",
                                             'ordering': 1,
                                             'summary': """Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna""",
                                             'text': """<p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
            Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
            Sed eu ultricies massa</p>""",
                                             'boards': [{'name': "Opening Scene Research",
                                                         'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                                         'notes': [{'title': "Location Ideas",
                                                                    "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                                    }],
                                                         'icon_name': "notepad",
                                                         'icon_url': "",
                                                         'color': "",
                                                         }],
                                             'sub_sections': [{'title': "Scene 1",
                                                               'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."},
                                                              {'title': "Scene 2",
                                                               'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."}],
                                             },
                                            {'title': "Chapter 3",
                                             'ordering': 2,
                                             'summary': """Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna""",
                                             'text': """<p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
            Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
            Sed eu ultricies massa</p>""",
                                             'boards': [{'name': "Opening Scene Research",
                                                         'summary': "",
                                                         'notes': [{'title': "Location Ideas",
                                                                    "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                                    }],
                                                         'icon_name': "notepad",
                                                         'icon_url': "",
                                                         'color': "",
                                                         }],
                                             'sub_sections': [{'title': "Scene 1",
                                                               'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."},
                                                              {'title': "Scene 2",
                                                               'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."}],
                                             },
                                            {'title': "Chapter 4",
                                             'ordering': 3,
                                             'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                             'text': """<p><b>Lorem</b> ipsum dolor sit amet, consectetur adipiscing elit.
            Praesent id dolor mattis, vestibulum turpis ac, luctus nulla. Duis eget est risus.
            Nunc vulputate, urna at maximus rutrum, eros magna volutpat risus, ut luctus urna neque eget dui.
            Suspendisse ante magna, finibus ut velit eu, tincidunt gravida risus. Aliquam vulputate arcu sed convallis hendrerit.
            Phasellus et iaculis magna. Phasellus eget mauris in felis viverra congue sit amet at lorem.
            Duis tempus sollicitudin quam. Vivamus tincidunt libero sit amet nibh vulputate, at ullamcorper eros tincidunt.
            Sed eu ultricies massa</p>""",
                                             'boards': [{'name': "Opening Scene Research",
                                                         'summary': "Curabitur maximus mi non nibh elementum ultricies.",
                                                         'notes': [{'title': "Location Ideas",
                                                                    "text": """Sed fermentum tempus nisl, lacinia sagittis ligula hendrerit sed.
                    Praesent urna nisi, sodales ac libero eget, ullamcorper malesuada est.
                    Donec tincidunt facilisis lacus. Curabitur maximus mi non nibh elementum ultricies.
                    Sed quis ex eget risus luctus vulputate quis vitae nulla. Aliquam in sapien a
                    ex volutpat commodo at eu odio.""",
                                                                    }],
                                                         'icon_name': "notepad",
                                                         'icon_url': "",
                                                         'color': "",
                                                         }],
                                             'sub_sections': [{'title': "Scene 1",
                                                               'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."},
                                                              {'title': "Scene 2",
                                                               'text': "Phasellus eget mauris in felis viverra congue sit amet at lorem. \
                Duis tempus sollicitudin quam."}],
                                             }],
                               }
