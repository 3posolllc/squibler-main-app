import io

from flask_restful import Resource, reqparse
from flask_jwt_extended import jwt_required, get_raw_jwt, fresh_jwt_required
from flask import send_file

from .converter import convert_html_string


parser = reqparse.RequestParser()
parser.add_argument(
    'html_content',
    help='This field cannot be blank',
    required=True
)
parser.add_argument(
    'format_type',
    help='This field cannot be blank',
    required=True
)
parser.add_argument(
    'user_id',
    help='This field cannot be blank',
    required=True
)


class ConvertHtmlString(Resource):

    @jwt_required
    def get(self):
        jwt = get_raw_jwt()
        return jwt

    @jwt_required
    def post(self):
        try:
            user_id = get_raw_jwt()['uuid']

            data = parser.parse_args()

            if user_id != data.pop('user_id'):
                return {
                    'message': 'Invalid user id'
                }, 403

            outputfile_path, format_type = convert_html_string(**data)

            return send_file(
                outputfile_path,
                mimetype='application/%s' % format_type,
                as_attachment=True,
                attachment_filename='%s.%s' % ("exportfile", format_type)
            )
        except Exception as e:
            return {
                'error': str(e)
            }, 500
