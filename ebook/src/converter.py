import subprocess


def convert_html_string(html_content, format_type):
    file_input_path = 'input.html'
    file_output_path = 'output.%s' % format_type

    f = open(file_input_path, 'w+')
    f.write(html_content)
    f.close()

    subprocess.call([
        "ebook-convert", file_input_path, file_output_path,
        "--margin-top=40", "--margin-bottom=50",
        "--margin-left=50", "--margin-right=50"
    ])

    return file_output_path, format_type
