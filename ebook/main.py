import logging

from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager
from flask_cors import CORS


app = Flask(__name__)
api = Api(app)
CORS(app)

app.config['JWT_SECRET_KEY'] = 'bllwu*#3q^ca3nmoxx+vx+&89$zri05xpo6&2u7i!pwssbfq5r'
app.config['JWT_HEADER_TYPE'] = 'JWT'
app.config['JWT_IDENTITY_CLAIM'] = 'uuid'
jwt = JWTManager(app)


from src import resources


api.add_resource(resources.ConvertHtmlString, '/convert')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)
