# Creating image based on official python3 image
FROM python:3.6
MAINTAINER Ostap Hyba

# Sets dumping log messages directly to stream instead of buffering
ENV PYTHONUNBUFFERED 1

# Creating and putting configurations
RUN mkdir /app
ADD . /app/
WORKDIR /app

# Install Calibre for converting files
RUN apt-get update
RUN apt-get install calibre -y
# RUN wget -nv -O- https://download.calibre-ebook.com/linux-installer.sh | sh /dev/stdin

# Installing all python dependencies
RUN pip install -r /app/requirements/development.txt

# Open port 8000 to outside world
EXPOSE 8000

# When container starts, this script will be executed.
# Note that it is NOT executed during building
CMD ["sh", "on-container-start.sh"]
